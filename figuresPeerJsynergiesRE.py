import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np



cot = pd.read_pickle('./CoT')
cot['velmsec'] = np.round(cot.vel/3.6,2)
energetic = pd.read_pickle('./energetic')
mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
Wint = pd.read_pickle('./wint')
mech['velmsec'] = np.round(mech.vel/3.6,2)
W = pd.read_pickle('./W')
mech.to_csv('./mech.csv')
W.to_csv('./W.csv')


colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}

fig1,ax1 = plt.subplots(2,4,figsize=(20,6))
g1 = sns.lineplot(x='velmsec',y='COT',data=cot,hue='gait', hue_order=['W','R','S'],ci='sd', ax=ax1[0,0],palette= colors,legend=False, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
g1.set_ylabel('COT (J/kg/m)')
g1.set(xlabel=None)
g1.set(xticklabels=[])
# sns.set()
sns.set_style("ticks")
sns.set_context("paper",font_scale = 1.3)
sns.despine()
# g.set_xticks(list(W.vel.unique()))
# leg_handles = g.get_legend_handles_labels()[0]
# g.legend(leg_handles,['Walking','Running','Skipping'],frameon=False)

g2 = sns.lineplot(x='velmsec',y='pot',data=cot,hue='gait', hue_order=['W','R','S'],ci='sd', ax=ax1[0,1],palette= colors,legend=False, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
g2.set_ylabel('Metabolic power (J/kg)')
sns.set_style("ticks")
sns.set_context("paper",font_scale = 1.3)
sns.despine()
g2.set(xlabel=None)
g2.set(xticklabels=[])


g3 = sns.lineplot(x='velmsec',y='recovery',hue='gait', hue_order=['W','R','S'],ci= 'sd', data=mech, ax= ax1[0,2],palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
leg_handles = g3.get_legend_handles_labels()[0]
g3.legend(leg_handles,['Walking','Running','Skipping'],frameon=False,loc='center left', bbox_to_anchor=(1.1, .6))
g3.set(xlabel=None)
g3.set(xticklabels=[])

ax1[0,3].axis('off') 

g4 = sns.lineplot(x='vel',y='wext',hue='gait', hue_order=['W','R','S'],ci= 'sd', data=W, ax= ax1[1,0], palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws,legend=False)
g5 = sns.lineplot(x='vel',y='wint',hue='gait', hue_order = ['R','S','W'],ci= 'sd', data=W, ax= ax1[1,1], palette=colors,dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws,legend=False)
g6 = sns.lineplot(x='vel',y='wtotal',hue='gait', hue_order = ['R','S','W'],ci= 'sd', data=W, ax= ax1[1,2], palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws,legend=False)



g3.set_ylabel('Recovery (%)')
g4.set_ylabel('Wext (J/Kg.m)')
g4.set_xlabel('Speed (m.sec$^{-1}$)')
g5.set_ylabel('Wint (J/Kg.m)')
g5.set_xlabel('Speed (m.sec$^{-1}$)')
g6.set_xlabel('Speed (m.sec$^{-1}$)')
g6.set_ylabel('Wtotal (J/Kg.m)')

ejex = np.round(np.sort(W.vel.unique()),1)

g4.set_xticks(ejex)
g4.set_xticklabels(ejex)
g5.set_xticks(ejex)
g5.set_xticklabels(ejex)
g6.set_xticks(ejex)
g6.set_xticklabels(ejex)

# g3.legend(title=None, loc='upper right')
# g3.legend_.set_frame_on(False)
# g3.legend_.set_title(None)

g1.text(1,6,'A',fontsize=20)
g2.text(1,13,'B',fontsize=20)

g3.text(1,75,'C',fontsize=20)
g3.text(1.39,75,'*',fontsize=12)
g3.text(1.81,75,'* $\dag$  $\S$',fontsize=12)
	

g4.text(1,1.65,'D',fontsize=20)
g4.text(1.39,1.8,'*',fontsize=12)
g4.text(1.78,1.8,'* $\S$ ',fontsize=12)
g4.text(2.5,1.8,r'$\dag$',fontsize=16)
# g1.text(6.6,1.8,r'$\S$',fontsize=16)
g4.text(2.5,1.8,r'$\dag$',fontsize=12)



# g2.text(,75,r'$\dag$, ',fontsize=16)
# g2.text(6.8,75,r'$\S$',fontsize=16)
g5.text(1,.58,'E',fontsize=20)
g5.text(2.5,42,r'$\dag$',fontsize=12)
g6.text(1,2,'F',fontsize=20)



plt.savefig('./cot.pdf')
plt.savefig('./cot.png')
# plt.close()

#Figure1
# dutyFactor = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/spatiotemporal')
# dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
# dutyLD.drop(['gait'], axis=1)
# dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
# dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
# dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
# dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
# df = pd.concat([dutyTR,dutyLD],ignore_index=True)
# df['velmsec'] = np.round(df.vel/3.6,2)

# energetic = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/energetic').reset_index()
# energetic['velmsec'] = energetic.vel/3.6
# DFT = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/DutyFlightTime')
# DFT['velmsec'] = DFT.vel/3.6

# fig1,ax1 = plt.subplots(1,4,figsize=(15,4))
# err_kws = {'elinewidth':1,'capsize':3}

# sns.set_theme()
# sns.despine()
# sns.set_context("paper")

# g10 = sns.lineplot(x='velmsec',y='strideLength',data=energetic,ax=ax1[0],ci='sd',hue='gait',palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
# ax1[0].set_xlabel('Speed (m.sec$^{-1}$)')
# ax1[0].set_ylabel('Stride Length (m)')
# ax1[0].text(1,2.2,'A',fontsize=16)

# ax1[0].text(1.3,2.1,'*',fontsize=10)
# ax1[0].text(1.7,2.1,'*,#,$\dag$',fontsize=10)
# ax1[0].text(2.4,2.1,'#',fontsize=10)

# legendg10 = g10.get_legend_handles_labels()[1]
# ax1[0].legend(legendg10,frameon=False, bbox_to_anchor=(-.15, 1))
# # ax1[0].set_xticklabels('Speed (m.sec$^{-1}$)')

# g11 = sns.lineplot(x='velmsec',y='strideFreq',data=energetic,ax=ax1[1],ci='sd',hue='gait',palette=colors,dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws,legend=False)
# ax1[1].set_xlabel('Speed (m.sec$^{-1}$)')
# ax1[1].set_ylabel('Stride Frequency (Hz)')
# ax1[1].text(1,2,'B',fontsize=16)
# ax1[1].text(1.3,1.9,'*',fontsize=10)
# ax1[1].text(1.7,1.9,'*,#,$\dag$',fontsize=10)
# ax1[1].text(2.3,1.9,'#',fontsize=10)

# g12 = sns.lineplot(x='velmsec',y='dutyFlight',ci='sd',hue='gait',data=DFT,ax=ax1[2], dashes=False,markers= ['o','o'], style="gait",palette=['#000000',"#1B9E77"],err_style = 'bars',err_kws=err_kws,legend=False)
# ax1[2].set_xlabel('Speed (m.sec$^{-1}$)')
# ax1[2].set_ylabel('Flight time \n (as a fraction of stride time)')
# ax1[2].text(1.5,.46,'C',fontsize=16)
# ax1[2].text(1.7,.42,'#',fontsize=10)

# sns.lineplot(x='velmsec',y='DutyFactor',data=df,ax=ax1[3],ci='sd',hue='gait',palette=colorsLegs,style="gait", dashes=False,markers= ['o','o','o','o'],err_style = 'bars',err_kws=err_kws)
# g13 = ax1[3].set_xlabel('Speed (m.sec$^{-1}$)')
# ax1[3].set_ylabel('Duty Factor')
# ax1[3].legend(['W','R','Str','Sld'],frameon=False)
# ax1[3].text(1,.8,'D',fontsize=16)

# ax1[3].text(1.2,.75,'$\ddag$,$\S$,$\yen$',fontsize=10)
# ax1[3].text(1.7,.75,'$\ddag$,$\S$,$\yen$,$\emptyset$,$\spadesuit$',fontsize=10)
# ax1[3].text(2.3,.5,'$\emptyset$,$\yen$',fontsize=10)

# plt.tight_layout(pad=1.5)
# plt.show()
# fig1.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig1PeerJsyn.png')
# fig1.savefig('/home/german//ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig1PeerJsyn.pdf')


# #Figure2
# vafN = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/VAF_2/nsynSeaborn')
# vafN['velmsec'] = np.round(vafN.vel/3.6,2)
# a= vafN.gait.to_list()
# for n, i in enumerate(a):
# 	if i == 'S*lead':
# 		a[n] = 'Slead'
# 	if i == 'S*tr':
# 		a[n] = 'Str'

# vafN['gait'] = a

# sns.set_theme()
# sns.set_context("paper")
# sns.set_style("white")
# g2 = sns.displot(vafN, x="nsyn", col="velmsec", hue='gait', hue_order=['W','R','Str','Slead'],multiple="dodge",
# facet_kws=dict(margin_titles=False), palette=colorsLegs, legend=True,
# )
# g2.fig.set_size_inches((12,3))
# msec = " m.sec$^{{-1}}$"
# g2.set_titles("{col_name}"+ msec)
# g2.legend.set_bbox_to_anchor((.12,.75))
# g2.legend.set_title(None)
# labels = ['W','R','Str','Slead']
# g2.legend.set_title(None)
# # g2.legend.texts = [(0, 0, 'W'), (0, 0, 'R'), (0, 0, 'Str'), (0, 0, 'Slead')]
# g2.set(ylabel='# of subjects',xlabel='# of synergies')

# # 
# # sns.despine()
# #

# plt.tight_layout()
# plt.show()
# g2.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig2PeerJsyn.png')
# g2.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig2PeerJsyn.pdf')