#!/usr/bin/env python
# coding: utf-8

# In[7]:


import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy import io
import peakutils
import tkinter as tk
from tkinter import filedialog
import os
import shutil
import glob


# In[8]:


#defino algunas cosas
#
# levanta archivo
# root = tk.Tk()
# root.withdraw()
# file_path = filedialog.askopenfilenames(initialdir = './emgCuttedSignals/',filetypes = [('numpyzip file','*.npz')])

folder = './emgCuttedSignals/'
file_path = glob.glob(folder+'*')

directory = './proc_for_synergies/'
if not os.path.exists(directory):
    os.makedirs(directory)

muscle_names = ('Gas','Sol','Rec', 'Vas','Tib', 'Bic', 'Glu')

for kk in file_path[:20]:
    filename = os.path.basename(kk)
    data = np.load(kk)
    tr = data['tr']
    emg = data['emg']
    fs_delsys = data['fs_delsys']
    stance_start_tr = data['heel_tr']
    stance_start_lead = data['heel_lead']
#     dt_delsys = 1/fs_delsys
#     t_delsys = np.arange(0,len(emg)*dt_delsys,dt_delsys)
    #===================================================================
    #Define parámetros de filtrado
    len_emg = len(emg[:,0])
    emg_offset = np.zeros((len_emg,14))
    # emg_n = np.zeros((len_emg,14))
    emg_smooth = np.zeros((len_emg,14))
    bb,aa = signal.butter(4,.04, 'highpass')
    B,A = signal.butter(4, .004)                     #Procesamiento basado en Clark, 2010
    #===================================================================

    #===================================================================
    #Procesamiento de la señal
    #pasabanda, pasalto rectificación y pasabajo (envolvente)
    #
    emg_pb= signal.filtfilt(bb,aa,emg,axis=0)# pasa-alto
    mean = np.mean(emg_pb,axis=0)
    emg_offset = emg_pb - mean                         #quita el offset
    emg_r = np.abs(emg_pb)                           # rectifica
    emg_smooth = signal.filtfilt(B,A,emg_r,axis=0)  # Suaviza, corre el pasabajo
    # emg_n = emg_smooth/np.max(emg_smooth,axis=0)    # normalizo de 0 a 1
    #===================================================================
    #Parte la emg en dos (leading y trailing)
    emgn = 2*[None]
    if tr == 'r':
        emgn[0] = emg_smooth[:,0:7]
        emgn[1] = emg_smooth[:,7:14]
    else:
        emgn[0] = emg_smooth[:,7:14]
        emgn[1] = emg_smooth[:,0:7]
    #===================================================================== 
    # Corta los ciclos, los interpola para que queden 100 puntos por ciclo y luego los concatena
    emgCycle = 2*[None]
    cycles = 2*[None]
    emgcnc = 2*[None]
   
    for index,leg,name in zip(range(2),[stance_start_tr,stance_start_lead],['_syn_tr_1','_syn_lead_1']):
        emgCycle[index] = np.zeros([len(stance_start_tr)-1,100,7])     #inicailiza variable
        for ii in range(len(stance_start_tr)-1):
            cycles[index] = emgn[index][leg[ii]:leg[ii+1],:]
            emgCycle[index][ii,:,:] = signal.resample(cycles[index],100,axis=0)     #interpola los ciclos para dejarlos de 100 puntos y los guarda en un array 3D (Esto es parte del proc standard que está en el artículo que cité arriba)
        emgcnc[index] = np.reshape(emgCycle[index],(100*(len(stance_start_tr)-1),7)).T                 #concatena los ciclos interpolados. También es parte del procesamiento standar. Sobre esta matriz aplicaríamos la factorización. Voy a seguir leyendo y buscar alguna implementación de PCA o NMF y luego disctuimos
        np.savez(directory+filename[:-4]+name,emg=emgcnc[index],emgcycle=emgCycle[index])

    
    #===================================================================== 
#     Gráficas
    #===================================================================== 

        # fig, axs = plt.subplots(7,1, figsize=(15, 6), sharex=True, facecolor='w', edgecolor='k')
        # fig.subplots_adjust(hspace = .001)
        # axs = axs.ravel()
        # for i in range(7):       
        #     axs[i].plot(emgn[index][:,i]/np.max(emgn[index][:,i]))
        #     axs[i].plot([leg-1,leg-1],[1,-1],'--',color = [.5, .5, .5],linewidth=0.5)
        #     axs[i].set_yticks([])
        #     axs[i].set_title(muscle_names[i],x = -.10,y = .5)
        # plt.suptitle(filename[0:-4] + name)
        # fig.savefig(directory+filename[:-4]+name+'.pdf')
        # plt.close()
    #=================================================================================================


