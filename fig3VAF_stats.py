import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols

nsyn = pd.read_pickle('./nsynn.pickle')

# kruskalGait = pg.kruskal(data=nsyn, dv='nsyn_elbow', between='gait').round(3)
# pg.print_table(kruskalGait)
# pairwiseGait = pg.pairwise_ttests(data=nsyn, dv='nsyn_elbow', between='gait',parametric=False,padjust='bonf')
# pg.print_table(pairwiseGait)
# kruskalVel = pg.kruskal(data=nsyn, dv='nsyn_elbow', between='vel').round(3)
# pg.print_table(kruskalVel)
# pairwiseVel = pg.pairwise_ttests(data=nsyn, dv='nsyn_elbow', between='vel',parametric=False,padjust='bonf')
# pg.print_table(pairwiseVel)


# kruskalCondition = pg.kruskal(data=nsyn, dv='nsyn_elbow', between='condition').round(3)
# pg.print_table(kruskalCondition)
# sub_nsyn = nsyn[~nsyn.condition.isin(['S_090_tr'])]
# pairwiseCondition = pg.pairwise_ttests(data=sub_nsyn, dv='nsyn_elbow', between='condition',parametric=False,padjust='bonf')
# pg.print_table(pairwiseCondition)


model = ols('nsyn_elbow~ C(gait)', data=nsyn[nsyn.vel==6.5]).fit()
norm = pg.normality(model.resid)
print(norm)
if ~norm.normal[0]:
	test = pg.kruskal(data=nsyn[nsyn.vel==6.5], dv='nsyn_elbow', between='gait').round(3)
else:
	test = pg.anova(data=nsyn[nsyn.vel==6.5], dv='nsyn_elbow', between='gait').round(3)
print(test)
if test['p-unc'][0]<0.05:
	pairwise = pg.pairwise_ttests(data=nsyn[nsyn.vel==6.5], dv='nsyn_elbow', between='gait',parametric=norm.normal[0],padjust='bonf')
	print(pairwise)
