import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np



cot = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/k5_data/CoT')
cot['velmsec'] = np.round(cot.vel/3.6,2)

colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

# plt.ioff()
plt.figure()
g = sns.pointplot(x='velmsec',y='COT',data=cot,hue='gait',palette= colors, ci='sd',errwidth=.7,capsize=.05,
	              hue_order=['W','R','S'])
g.set_xlabel('Speed (m.sec$^{-1}$)')
g.set_ylabel('COT (J/kg/m)')
sns.set()
sns.set_style("ticks")
sns.set_context("paper",font_scale = 1.3)
sns.despine()
leg_handles = g.get_legend_handles_labels()[0]
g.legend(leg_handles,['Walking','Running','Skipping'],frameon=False)
# plt.show()
plt.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/cot.pdf')
plt.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/cot.png')
# plt.close()

#Figure1
dutyFactor = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/spatiotemporal')
dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
dutyLD.drop(['gait'], axis=1)
dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
df = pd.concat([dutyTR,dutyLD],ignore_index=True)
df['velmsec'] = np.round(df.vel/3.6,2)

energetic = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/energetic').reset_index()
energetic['velmsec'] = energetic.vel/3.6
DFT = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/spatiotemporal/DutyFlightTime')
DFT['velmsec'] = DFT.vel/3.6

fig1,ax1 = plt.subplots(1,4,figsize=(15,4))
err_kws = {'elinewidth':1,'capsize':3}

sns.set_theme()
sns.despine()
sns.set_context("paper")

g10 = sns.lineplot(x='velmsec',y='strideLength',data=energetic,ax=ax1[0],ci='sd',hue='gait',palette=colors, dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws)
ax1[0].set_xlabel('Speed (m.sec$^{-1}$)')
ax1[0].set_ylabel('Stride Length (m)')
ax1[0].text(1,2.2,'A',fontsize=16)

ax1[0].text(1.3,2.1,'*',fontsize=10)
ax1[0].text(1.7,2.1,'*,#,$\dag$',fontsize=10)
ax1[0].text(2.4,2.1,'#',fontsize=10)

legendg10 = g10.get_legend_handles_labels()[1]
ax1[0].legend(legendg10,frameon=False, bbox_to_anchor=(-.15, 1))
# ax1[0].set_xticklabels('Speed (m.sec$^{-1}$)')

g11 = sns.lineplot(x='velmsec',y='strideFreq',data=energetic,ax=ax1[1],ci='sd',hue='gait',palette=colors,dashes=False,markers= ['o','o','o'], style="gait",err_style = 'bars',err_kws=err_kws,legend=False)
ax1[1].set_xlabel('Speed (m.sec$^{-1}$)')
ax1[1].set_ylabel('Stride Frequency (Hz)')
ax1[1].text(1,2,'B',fontsize=16)
ax1[1].text(1.3,1.9,'*',fontsize=10)
ax1[1].text(1.7,1.9,'*,#,$\dag$',fontsize=10)
ax1[1].text(2.3,1.9,'#',fontsize=10)

g12 = sns.lineplot(x='velmsec',y='dutyFlight',ci='sd',hue='gait',data=DFT,ax=ax1[2], dashes=False,markers= ['o','o'], style="gait",palette=['#000000',"#1B9E77"],err_style = 'bars',err_kws=err_kws,legend=False)
ax1[2].set_xlabel('Speed (m.sec$^{-1}$)')
ax1[2].set_ylabel('Flight time \n (as a fraction of stride time)')
ax1[2].text(1.5,.46,'C',fontsize=16)
ax1[2].text(1.7,.42,'#',fontsize=10)

sns.lineplot(x='velmsec',y='DutyFactor',data=df,ax=ax1[3],ci='sd',hue='gait',palette=colorsLegs,style="gait", dashes=False,markers= ['o','o','o','o'],err_style = 'bars',err_kws=err_kws)
g13 = ax1[3].set_xlabel('Speed (m.sec$^{-1}$)')
ax1[3].set_ylabel('Duty Factor')
ax1[3].legend(['W','R','Str','Sld'],frameon=False)
ax1[3].text(1,.8,'D',fontsize=16)

ax1[3].text(1.2,.75,'$\ddag$,$\S$,$\yen$',fontsize=10)
ax1[3].text(1.7,.75,'$\ddag$,$\S$,$\yen$,$\emptyset$,$\spadesuit$',fontsize=10)
ax1[3].text(2.3,.5,'$\emptyset$,$\yen$',fontsize=10)

plt.tight_layout(pad=1.5)
plt.show()
fig1.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig1PeerJsyn.png')
fig1.savefig('/home/german//ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig1PeerJsyn.pdf')


#Figure2
vafN = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/emg/VAF_2/nsynSeaborn')
vafN['velmsec'] = np.round(vafN.vel/3.6,2)
a= vafN.gait.to_list()
for n, i in enumerate(a):
	if i == 'S*lead':
		a[n] = 'Slead'
	if i == 'S*tr':
		a[n] = 'Str'

vafN['gait'] = a

sns.set_theme()
sns.set_context("paper")
sns.set_style("white")
g2 = sns.displot(vafN, x="nsyn", col="velmsec", hue='gait', hue_order=['W','R','Str','Slead'],multiple="dodge",
facet_kws=dict(margin_titles=False), palette=colorsLegs, legend=True,
)
g2.fig.set_size_inches((12,3))
msec = " m.sec$^{{-1}}$"
g2.set_titles("{col_name}"+ msec)
g2.legend.set_bbox_to_anchor((.12,.75))
g2.legend.set_title(None)
labels = ['W','R','Str','Slead']
g2.legend.set_title(None)
# g2.legend.texts = [(0, 0, 'W'), (0, 0, 'R'), (0, 0, 'Str'), (0, 0, 'Slead')]
g2.set(ylabel='# of subjects',xlabel='# of synergies')

# 
# sns.despine()
#

plt.tight_layout()
plt.show()
g2.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig2PeerJsyn.png')
g2.savefig('/home/german/ownCloud/work/phd/proyect_2016/paper/peerJsynergies/fig2PeerJsyn.pdf')