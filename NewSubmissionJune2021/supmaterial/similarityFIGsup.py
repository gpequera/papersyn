#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
import seaborn as sns
from pingouin import ttest
from itertools import combinations


cosim = pd.read_pickle("./cosim")


listdfa = []
listdfb = []
for comparacion in list(cosim.comp.unique()):
    tt = cosim[cosim.comp.isin([comparacion])]
    for v in list(tt.vel.unique()):
        ttt = tt[tt.vel.isin([v])]
        for col in list(ttt.syn.unique()):
            dfa = pd.DataFrame()
            dfb = pd.DataFrame()
            
            tabla = ttt[ttt.syn.isin([col])]
            
            a = ttest(tabla['cosim'],.45,tail='greater')['p-val'].round(3).item()
            dfa['pval'] = [a]
            dfa['syn'] = col
            dfa['comp'] = comparacion
            dfa['vel'] = [v]
            listdfa.append(dfa)
            
            b = ttest(tabla['cosim'],.6,tail='greater')['p-val'].round(3).item()
            dfb['pval'] = [b]
            dfb['syn'] = col
            dfb['comp'] = comparacion
            dfb['vel'] = [v]
            listdfb.append(dfb)
            
DF1 = pd.concat(listdfa,ignore_index=True)
DF2 = pd.concat(listdfb,ignore_index=True)


# In[16]:


significance1 = DF1[DF1.pval>.05]
significance2 = DF2[DF2.pval>.05]
synmap = [['M1', 'M2','M3','M4'],[-.3, -.1, .1, .3]]
velmap = [[5,6.5,9],[0,1,2]]


fig = plt.figure(figsize=(10,20))
sns.set_theme()
sns.set(style="white", font_scale=1.5)

conditions = ['Slead_vs_Wtr','Rtr_vs_Slead','Rtr_vs_Wtr','Str_vs_Wtr','Rtr_vs_Str','Slead_vs_Str']
titles = ['Slead vs W','R vs Slead','R vs W','Str vs W','R vs Str', 'Slead vs Str']
splot = sns.catplot(x='vel',y='cosim',hue='syn',col='comp',col_wrap=3,legend=True,
                    data=cosim[cosim.comp.isin(conditions)],col_order=conditions,kind='bar',palette="crest",aspect=.7)
splot.fig.set_size_inches((12,4))
for ax, title in zip(splot.axes.flat,titles):
    ax.set_title(title)

splot.legend.set_bbox_to_anchor((1.05,.9))    
splot.legend.set_title(None)    
    
for i, cond in zip(range(len(conditions)),conditions):
    asterisco = significance2[significance2.comp.isin([cond])]
    for index, row in asterisco.iterrows():
        x1 = synmap[1][synmap[0].index(row['syn'])]
        x2 = velmap[1][velmap[0].index(row['vel'])]
        splot.axes[i].annotate('*', 
                       (x1+x2,1), 
                       ha = 'center', va = 'center', 
                       size=15,
                       xytext = (0, -12), 
                       textcoords = 'offset points')

plt.tight_layout(pad=.1)

splot.axes[0].set_ylabel('cosim',fontsize=16)
splot.axes[3].set_ylabel('cosim',fontsize=16)

splot.axes[3].set_xticklabels([1.39, 1.81, 2.5],fontsize=16)


splot.axes[3].set_xlabel('Speed (m.s$^{-1}$)',fontsize=16)
splot.axes[4].set_xlabel('Speed (m.s$^{-1}$)',fontsize=16)
splot.axes[5].set_xlabel('Speed (m.s$^{-1}$)',fontsize=16)

splot.savefig('./NewSubmissionJune2021/supmaterial/similarityFIGsup.pdf', format='pdf',bbox_inches='tight')
