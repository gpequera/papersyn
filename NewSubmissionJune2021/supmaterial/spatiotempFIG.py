import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np

dutyFactor = pd.read_pickle('./spatiotemporal')
dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
dutyLD.drop(['gait'], axis=1)
dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
df = pd.concat([dutyTR,dutyLD],ignore_index=True)
df['velmsec'] = np.round(df.vel/3.6,2)

energetic = pd.read_pickle('./energetic').reset_index()
energetic['velmsec'] = np.round(energetic.vel/3.6,2)
DFT = pd.read_pickle('./DutyFlightTime')
DFT['velmsec'] = DFT.vel/3.6

mech_regressions = pd.read_pickle('./reg_spt.pickle')
duty_regressions = pd.read_pickle('./reg_duty.pickle')
ft_regressions = pd.read_pickle('./reg_ft.pickle')

colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]


fig1,ax1 = plt.subplots(2,2,figsize=(10,8), sharex=True)
err_kws = {'elinewidth':1,'capsize':3}

sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()


g10 = sns.scatterplot(x='velmsec',y='strideLength',data=energetic,ax=ax1[0,0],hue='gait',palette=colors,s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['strideLength'])],hue='gait', hue_order=['W','R','S'], ax=ax1[0,0],palette= colors,ci=95)

ax1[0,0].set_xlabel('Speed (m.s$^{-1}$)')
ax1[0,0].set_ylabel('Stride Length (m)')
g10.set(xlabel=None)
g10.set(xticklabels=[])

ax1[0,0].text(.8,2.4,'A',fontsize=16)
# ax1[0,0].text(1.3,2.4,'*',fontsize=10)
# ax1[0,0].text(1.7,2.4,'* # $\dag$',fontsize=10)
# ax1[0,0].text(2.5,2.4,'$\dag$',fontsize=10)
leg_handles = g10.get_legend_handles_labels()[0]
g10.legend(leg_handles,['Walking','Running','Skipping'],frameon=False, bbox_to_anchor=(-.2, 1))

g11 = sns.scatterplot(x='velmsec',y='strideFreq',data=energetic,ax=ax1[0,1],hue='gait',palette=colors,s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['strideFreq'])],hue='gait', hue_order=['W','R','S'], ax=ax1[0,1],palette= colors,ci=95)
ax1[0,1].set_xlabel('Speed (m.s$^{-1}$)')
ax1[0,1].set_ylabel('Stride Frequency (Hz)')
ax1[0,1].text(.8,2.2,'B',fontsize=16)
# ax1[0,1].text(1.3,2.2,'*',fontsize=10)
# ax1[0,1].text(1.7,2.2,'* # $\dag$',fontsize=10)
# ax1[0,1].text(2.4,2.2,'$\dag$',fontsize=10)
g11.set(xlabel=None)
g11.set(xticklabels=[])
g11.legend_.remove()

g13 = sns.scatterplot(x='velmsec',y='DutyFactor',data=df,ax=ax1[1,0], hue='gait',palette=colorsLegs,s=10)#, dashes=False,markers= ['o','o','o','o'],err_style = 'bars',err_kws=err_kws)
sns.lineplot(x='velmsec',y='values2',data=duty_regressions,hue='gait', hue_order=['W','R','S', 'Sld'], ax=ax1[1,0],palette= colorsLegs,ci=95)
ax1[1,0].set_xlabel('Speed (m.s$^{-1}$)')
ax1[1,0].set_ylabel('Duty Factor')
leg_handles = g13.get_legend_handles_labels()[0]
g13.legend(leg_handles,['Walking','Running','Skipping\n trailing','Skipping\n leading' ],frameon=False, bbox_to_anchor=(1.2, 1))
ejex = np.round(np.sort(energetic.velmsec.unique()),1)
g13.set_xticks(ejex)
g13.set_xticklabels(ejex)
ax1[1,0].text(.8,.9,'C',fontsize=16)
# ax1[1,0].text(1.4,.9,'$\ddag$ $\yen$',fontsize=10)
# ax1[1,0].text(1.7,.9,'$\ddag$ # $\yen$ $\S$ $\emptyset$ ',fontsize=10)
# ax1[1,0].text(2.5,.9,'$\S$ $\emptyset$ ',fontsize=10)

g12 = sns.scatterplot(x='velmsec',y='dutyFlight',ci=95,hue='gait',data=DFT,ax=ax1[1,1],palette=['#000000',"#1B9E77"], legend=False,s=10	)
sns.lineplot(x='velmsec',y='values2',data=ft_regressions,hue='gait', hue_order=['S', 'R'], ax=ax1[1,1],palette= ['#000000',"#1B9E77"],ci=95)
# ax1[1,1].axis('off')
ax1[1,1].set_ylabel('Flight time \n (as a fraction of stride time)')
ax1[1,1].text(.8,.55,'D',fontsize=16)
ax1[1,1].set_xlabel('Speed (m.s$^{-1}$)')
# ax1[1,1].text(1.8,.55,'$\dag$',fontsize=10)
# ax1[1,1].text(2.5,.55,'$\dag$',fontsize=10)
g12.set_xticks(ejex)
g12.set_xticklabels(ejex)
g12.legend_.remove()


plt.tight_layout(pad=1.5)
fig1.savefig('./NewSubmissionJune2021/supmaterial/spatiotemporalFIGsup.pdf')
plt.close()
