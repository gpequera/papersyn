import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols
import numpy as np

mech = pd.read_csv('./mech.csv')
W = pd.read_pickle('./W')
dutyFactor = pd.read_pickle('./spatiotemporal')
dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
dutyLD.drop(['gait'], axis=1)
dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
DF = pd.concat([dutyTR,dutyLD],ignore_index=True)
DF['velmsec'] = np.round(DF.vel/3.6,2)
DFT = pd.read_pickle('./DutyFlightTime')
DFT['velmsec'] = np.round(DFT.vel/3.6,2)




mech5 = mech[mech.vel==5]
mech65 = mech[mech.vel==6.5]
mech9 = mech[mech.vel==9]
mechR = mech[mech.gait=='R']
mechW = mech[mech.gait=='W']
mechS = mech[mech.gait=='S']

W['velmsec'] = W['vel']

W5 = W[W.vel==1.39]
W65 = W[W.vel==1.81]
W9 = W[W.vel==2.5]
WR = W[W.gait=='R']
WS = W[W.gait=='S']
WW = W[W.gait=='W']

DF5 = DF[DF.vel==5]
DF65 = DF[DF.vel==6.5]
DF9 = DF[DF.vel==9]
DFW = DF[DF.gait=='W']
DFStr = DF[DF.gait=='S']
DFSld = DF[DF.gait=='Sld']
DFR = DF[DF.gait=='R']

DFT65 = DFT[DFT.vel==6.5]
DFT9 = DFT[DFT.vel==9]
DFTR = DFT[DFT.gait=='R']
DFTS = DFT[DFT.gait=='S']


lista = []
listalevene = []
for vel in [1.39,1.81,2.5]:
	for var1 in ['wext', 'wint', 'wtotal']:
		formula = var1+'~ C(gait)'
		model = ols(formula, data=W[W.velmsec==vel]).fit()
		norm = pg.normality(model.resid)
		norm['vel'] = [vel]
		norm['var'] = [var1]
		levene = pg.homoscedasticity(W[W.velmsec==vel], dv=var1, group='gait')
		levene['vel'] = [vel]
		levene['var'] = [var1]
		norm['levene'] = levene['equal_var'].item()
		lista.append(norm)
		listalevene.append(levene)
	for var2 in ['recovery', 'strideFreq', 'strideLength']:
		formula = var2+'~ C(gait)'
		model = ols(formula, data=mech[mech.velmsec==vel]).fit()
		norm = pg.normality(model.resid)
		norm['vel'] = [vel]
		norm['var'] = [var2]
		levene = pg.homoscedasticity(mech[mech.velmsec==vel], dv=var2, group='gait')
		levene['vel'] = [vel]
		levene['var'] = [var2]
		norm['levene'] = levene['equal_var'].item()
		lista.append(norm)
		listalevene.append(levene)
	model = ols('DutyFactor~ C(gait)', data=DF[DF.velmsec==vel]).fit()
	norm = pg.normality(model.resid)
	norm['vel'] = [vel]
	norm['var'] = ['DutyFactor']
	levene = pg.homoscedasticity(DF[DF.velmsec==vel], dv='DutyFactor', group='gait')
	norm['levene'] = levene.equal_var.item()
	levene['vel'] = [vel]
	levene['var'] = ['DutyFactor']
	lista.append(norm)
	listalevene.append(levene)

for vel in [1.81,2.5]:
	model = ols('dutyFlight~ C(gait)', data=DFT[DFT.velmsec==vel]).fit()
	norm = pg.normality(model.resid)
	norm['vel'] = [vel]
	norm['var'] = ['dutyFlight']
	levene = pg.homoscedasticity(DFT[DFT.velmsec==vel], dv='dutyFlight', group='gait')
	norm['levene'] = levene.equal_var.item()
	levene['vel'] = [vel]
	levene['var'] = ['dutyFlight']
	lista.append(norm)
	listalevene.append(levene)

normal = pd.concat(lista,ignore_index=True)
normal = normal[['var', 'vel','W', 'pval', 'normal', 'levene']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
levene_test = pd.concat(listalevene,ignore_index=True)
levene_test = levene_test[['var', 'vel', 'W', 'pval', 'equal_var']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
# for vel in [1.81,2.5]:
listaanovas = []
listakrukals = []
listapairwises = []
for index, row in normal.iterrows():
	dv = row['var']
	vel = row['vel']
	for data in [W,mech,DF,DFT]:
		subset= data[data.velmsec==vel]
		if dv in list(data.columns):
			if  len(subset.gait.unique())>2:
				if row['normal'] == True:
					if row['levene'] == False:
						anovaGait = pg.welch_anova(data=subset, dv=dv, between='gait').round(3)
						anovaGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
						anovaGait['var'] = [str(dv)]
						anovaGait['vel'] = [str(vel)]
						listaanovas.append(anovaGait)
					else:
						anovaGait = pg.anova(data=subset, dv=dv, between='gait', detailed=True).round(3).drop([1])
						anovaGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
						anovaGait['var'] = [str(dv)]
						anovaGait['vel'] = [str(vel)]
						listaanovas.append(anovaGait)
					if anovaGait['p-unc'][0]<0.05:
						pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=True).round(3)
						pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
						pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
						pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
						listapairwises.append(pairwiseGait)
				else:
					kruskalGait = pg.kruskal(data=subset, dv=dv, between='gait', detailed=True).round(3)
					kruskalGait['condition'] = [str(dv)+'-'+'- '+str(vel)]
					kruskalGait['var'] = [str(dv)]
					kruskalGait['vel'] = [str(vel)]
					listakrukals.append(kruskalGait)
					if kruskalGait['p-unc'][0]<0.05:
						pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=False).round(3)
						pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
						pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
						pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
						listapairwises.append(pairwiseGait)
			else:
				pairwiseGait = pg.pairwise_ttests(dv=dv, between='gait', data=subset, padjust='bonf',parametric=row['normal']).round(3)
				pairwiseGait['condition'] = [str(dv)+'-'+'- '+str(vel)] * pairwiseGait.shape[0]
				pairwiseGait['var'] = [str(dv)]  * pairwiseGait.shape[0]
				pairwiseGait['vel'] = [str(vel)] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)


anovas = pd.concat(listaanovas,ignore_index=True).round(3)
kruskals = pd.concat(listakrukals,ignore_index=True).round(3)
pairwises = pd.concat(listapairwises,ignore_index=True).round(3)

for letra in ['A','B']:
	pairwises[letra] = pairwises[letra].replace('S','Str')		

pairwises = pairwises[['var','vel','Contrast','A','B','Parametric','p-unc','p-corr','p-adjust']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
kruskals = kruskals[['var', 'vel','Source', 'ddof1', 'H', 'p-unc']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)
anovas = anovas[['var', 'vel','Source', 'SS', 'DF', 'MS', 'F', 'p-unc', 'np2', 'ddof1','ddof2']].sort_values(by=['vel','var']).round(3).reset_index(drop=True)