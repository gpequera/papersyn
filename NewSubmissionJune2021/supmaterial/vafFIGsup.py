import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

vaf = pd.read_pickle('VAF.pickle')
nsyn = pd.read_pickle('./nsynn.pickle')
nsyn['velmsec'] = (nsyn.vel/3.6).round(2)
vaf['evar100'] = (vaf.evar)*100


fig,ax = plt.subplots(1,figsize=(9,7))
# ax = ax.ravel()

colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]
hue_order=['Wtr','Rtr','Str','Sld']
legend = ['Walking','Running','Skipping\n trailing','Skipping\n leading' ]

sns.despine()

# sns.lineplot(x='rango',y='evar100',hue='condition',data=vaf,ax=ax[0])
# ax[0].set_ylabel('VAF (%)')
# ax[0].set_xlabel('Number of synergies')
# handles, labels = ax[0].get_legend_handles_labels()
# ax[0].legend(handles, labels, bbox_to_anchor=(-.12, 1), borderaxespad=0.,frameon=False)

# sns.stripplot(x='velmsec',y='nsyn_elbow',data=nsyn,hue='gait',ax=ax[1],
				# dodge=True,linewidth=.5,palette=colorsLegs,hue_order=hue_order,
				# size=4)
sns.barplot(x='velmsec',y='nsyn_elbow',data=nsyn,hue='gait',ci='sd',
						ax=ax,hue_order=hue_order,palette=colorsLegs,linewidth=2,
                 		errcolor=".2", edgecolor=".2",capsize = 0.1,errwidth=.5)

handles, labels = ax.get_legend_handles_labels()
lgd = ax.legend(handles, legend, bbox_to_anchor=(1, 1),frameon=False)

# barplot.legend(hue_order)
# barplot.legend_.remove()
ax.set_ylim([0,5])
ax.set_ylabel('Number of synergies')
ax.set_xlabel('Speed (m.s$^{{-1}}$)')

fig.savefig('./NewSubmissionJune2021/supmaterial/vafFIGsup.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
# plt.show()
