import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits import mplot3d
from scipy import signal


Sld = np.load('GC_S_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_l')[7]
Str = np.load('GC_S_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]
R = np.load('GC_R_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]
W = np.load('GC_W_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]

gait=Sld
lines = {}

fig = plt.figure(figsize=(6,2.5))
n_sticks = 5
# for g,gait in enumerate([W,R,Str,Sld]):
for f,i in enumerate(range(0,100,int(100/n_sticks))):
	ax = fig.add_subplot(1, n_sticks, f+1, projection='3d')
	for side in ['R','L']:
		for coord in ['x', 'y', 'z']:
			m_upper = gait[[side+'SH'+coord, side+'ELB'+coord, side+'WR'+coord]]
			m_lower = gait[[side+'GT'+coord, side+'KN'+coord, side+'AN'+coord, side+'MT'+coord]]
			lines.update({'upper_'+side+coord:pd.DataFrame(signal.resample(m_upper.to_numpy(),100),columns=m_upper.columns)})
			lines.update({'lower_'+side+coord:pd.DataFrame(signal.resample(m_lower.to_numpy(),100),columns=m_lower.columns)})
		for region in ['upper_', 'lower_']:
			if side == 'L':
				style = '-k'
			else:
				style = ':k'
			line1, = ax.plot3D(lines[region+side+'x'].iloc[i,:],lines[region+side+'y'].iloc[i,:],lines[region+side+'z'].iloc[i,:],style,linewidth=2)
			ax.view_init(0, 0, )
			ax.set_xlim([0,1500])
			ax.axis('off')

fig.subplots_adjust(wspace=0,right=1,bottom=0,top=1,left=0)	
plt.show()
plt.savefig('./animate.svg')

