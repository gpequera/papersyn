import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
from mpl_toolkits import mplot3d
from scipy import signal

f = open('emg.pickle','rb') 
data_dict = pickle.load(f) 
f.close() 

spt = pd.read_pickle('./spt')
sub = spt.groupby(['vel','gait','leg']).mean().loc[6.5]['dutyFactor']
duty= {'Sld':sub.loc['S'].loc['ld'],'Str':sub.loc['S'].loc['tr'],'W':sub.loc['W'].loc['tr'],'R':sub.loc['R'].loc['tr'] }

muscle_names = muscles = ['Gas','Sol','RF','Vas','Tib','BF','GluM']

gaits = ['W', 'R', 'Str', 'Sld']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]
means = {}
stds = {}

Wkeys = [s for s in data_dict.keys() if 'W_065_tr' in s]
Rkeys = [s for s in data_dict.keys() if 'R_065_tr' in s]
Strkeys = [s for s in data_dict.keys() if 'S_065_tr' in s]
Sldkeys = [s for s in data_dict.keys() if 'S_065_ld' in s]
keys = [Wkeys, Rkeys, Strkeys,Sldkeys]

Sld = np.load('GC_S_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_l')[7]
Str = np.load('GC_S_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]
R = np.load('GC_R_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]
W = np.load('GC_W_065.npy',allow_pickle=True,encoding='latin1').item().get('kinematic_r')[7]
datas = [W,R,Str,Sld]
n_sticks = 5
lines = {}

for key,gait in zip(keys,gaits):
	lista= []
	for i in range(len(key)):
		data = data_dict[key[i]]
		m = np.split(data.to_numpy(),data.shape[0]/100)
		lista.append(np.mean(m,axis=0))
	means.update({gait:pd.DataFrame(np.mean(lista,axis=0),columns=muscle_names)})
	stds.update({gait:pd.DataFrame(np.std(lista,axis=0),columns=muscle_names)})

fig10 = plt.figure(figsize=(8,8))
gs0 = fig10.add_gridspec(8,4)

for j,gait in enumerate(['Walking', 'Running', 'Skipping trailing','Skipping leading']):
	gs00 = gs0[0,j].subgridspec(1,n_sticks)
	for a,i in enumerate(range(0,100,int(100/n_sticks))):
		# fig10.add_subplot(gs00[a, b])
		ax = fig10.add_subplot(gs00[0,a], projection='3d')
		if gait == 'Skipping leading':
			styles = [':k','-k']
		else:
			styles = ['-k',':k']
		for side, style in zip(['R','L'],styles):
			for coord in ['x', 'y', 'z']:
				m_upper = datas[j][[side+'SH'+coord, side+'ELB'+coord, side+'WR'+coord]]
				m_lower = datas[j][[side+'GT'+coord, side+'KN'+coord, side+'AN'+coord, side+'MT'+coord]]
				lines.update({'upper_'+side+coord:pd.DataFrame(signal.resample(m_upper.to_numpy(),100),columns=m_upper.columns)})
				lines.update({'lower_'+side+coord:pd.DataFrame(signal.resample(m_lower.to_numpy(),100),columns=m_lower.columns)})
			for region in ['upper_', 'lower_']:
				line1, = ax.plot3D(lines[region+side+'x'].iloc[i,:],lines[region+side+'y'].iloc[i,:],lines[region+side+'z'].iloc[i,:],style,linewidth=.5)
				ax.view_init(0, 0, )
				ax.set_xlim([0,1500])
				ax.axis('off')
		if a ==2:
			# ax.text(0.5, 1.08, gait,horizontalalignment='center',fontsize=12)
			ax.set_title(gait,verticalalignment='center', horizontalalignment='center',fontsize=12, pad=20)
	for b,muscle in enumerate(muscle_names):
		ax1 = fig10.add_subplot(gs0[b+1,j])
		y = means[gaits[j]][muscle]
		ystd = stds[gaits[j]][muscle]
		ax1.plot(y,colorsLegs[j])
		ax1.fill_between(np.arange(0,100),y-ystd,y+ystd,color=colorsLegs[j],alpha=.2)
		ax1.plot([duty[gaits[j]],duty[gaits[j]]],[0,1.2],color=(.8,.8,.8))
		ax1.set_ylim([0,1.2])
		ax1.axis('off')
		if j == 3:
			ax1.text(130,.45, muscle,verticalalignment='center', horizontalalignment='center')
		if j==0 and b==6:
			ax1.axis('on')
			ax1.spines['top'].set_visible(False)
			ax1.spines['right'].set_visible(False)
			ax1.set_xlabel('% gait cycle')
			ax1.set_ylabel('A.U.')
			ax1.set_yticks([0,1])
			ax1.set_yticklabels([0,1])

plt.show()
plt.savefig('./NewSubmissionJune2021/fig1.pdf')
