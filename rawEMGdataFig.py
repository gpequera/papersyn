import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle
import matplotlib.gridspec as gridspec



f = open('emg.pickle','rb') 
data_dict = pickle.load(f) 
f.close() 

spt = pd.read_pickle('./spt')
sub = spt.groupby(['vel','gait','leg']).mean().loc[6.5]['dutyFactor']
duty= {'Sld':sub.loc['S'].loc['ld'],'Str':sub.loc['S'].loc['tr'],'W':sub.loc['W'].loc['tr'],'R':sub.loc['R'].loc['tr'] }

labels = ['Walking','Running','Skipping\n trailing','Skipping\n leading' ]
muscle_names = list(data_dict['GP_W_065_tr'].columns)
gaits = ['W', 'R', 'Str', 'Sld']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]
means = {}
stds = {}

Wkeys = [s for s in data_dict.keys() if 'W_065_tr' in s]
Rkeys = [s for s in data_dict.keys() if 'R_065_tr' in s]
Strkeys = [s for s in data_dict.keys() if 'S_065_tr' in s]
Sldkeys = [s for s in data_dict.keys() if 'S_065_ld' in s]
keys = [Wkeys, Rkeys, Strkeys,Sldkeys]

for key,gait in zip(keys,gaits):
	lista= []
	for i in range(len(key)):
		data = data_dict[key[i]]
		m = np.split(data.to_numpy(),data.shape[0]/100)
		lista.append(np.mean(m,axis=0))
	means.update({gait:pd.DataFrame(np.mean(lista,axis=0),columns=muscle_names)})
	stds.update({gait:pd.DataFrame(np.std(lista,axis=0),columns=muscle_names)})

fig, ax = plt.subplots(7,4, sharex=True, sharey=True)
fig.subplots_adjust(hspace=-.05)
for col, gait in enumerate(gaits):
	for row,muscle in enumerate(muscle_names):
		y = means[gait][muscle]
		ystd = stds[gait][muscle]
		ax[row,col].plot(y,colorsLegs[col],label=labels[col])
		ax[row,col].fill_between(np.arange(0,100),y-ystd,y+ystd,color=colorsLegs[col],alpha=.2)
		ax[row,col].plot([duty[gait],duty[gait]],[0,1],color=(.8,.8,.8))
		ax[row,col].axis('off')
		if row == 0:
			ax[row,col].text(50, 1.5, labels[col],verticalalignment='center', horizontalalignment='center')
		if col == 3:
			ax[row,col].text(130,.45, muscle,verticalalignment='center', horizontalalignment='center')

ax[6,0].axis('on')
ax[6,0].spines['top'].set_visible(False)
ax[6,0].spines['right'].set_visible(False)
ax[6,0].set_xlabel('% gait cycle')
ax[6,0].set_ylabel('U.A.')
ax[6,0].set_yticks([0,1])
ax[6,0].set_yticklabels([0,1])

plt.show()
plt.savefig('./rawEMG.svg')

