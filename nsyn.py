import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

 #'The number of muscle synergies underlying each datasetwas  defined  as  the  minimum  number  of  synergies  required  toachieve  a  global  VAF  >  95%  and  a  mean  VAF  for  each  muscle(muscle VAF) that exceeded 80%. (Hagio, 2015)'

#'The number of synergies underlying each dataset was defined as the minimum number of synergies required to achieve a mean (n = 100) global VAF >90%, while satisfying local criteria of fit (see below), subject to the requirement that adding'
# 'another synergy increased the mean global VAF <3%. As local criteria, we required the mean VAF for each muscle (muscle VAF) and subtask (load level or position VAF) to exceed 85% (Roh, 2012)'



VAF = pd.read_pickle('VAF.pickle')
conditions = list(VAF.condition.unique())
#
#Métodos para VAF
#
#
##slope
#
lista = []
for cond in  conditions:
	for ss in VAF.subsample.unique():
		data = VAF[VAF.subsample.isin([ss]) & VAF.condition.isin([cond])]
		datax = data.rango.to_numpy().reshape(-1,1)
		datay = data.evar.to_numpy()
		regr = linear_model.LinearRegression()
		mse = []
		for i in range(6):								#ajuste de modelo lineal
			regr.fit(datax[i:], datay[i:])
			vaf_pred = regr.predict(datax[i:])
			mse.append(mean_squared_error(datay[i:],vaf_pred))
		nsyn = np.argmax(np.array(mse)<1e-4)+1
		nsyn3 = (np.argmax(np.diff(datay)<.03)) +1
		nsyn95 = np.argmax((datay>.95))+1
		nsyn95_3 = np.max([nsyn3,nsyn95])
		df = pd.DataFrame()
		df['condition'] = [cond]
		df['vel'] = [np.float(cond[2:5])/10]
		df['leg'] = [cond[-2:]]
		df['gait'] = [cond[0] + cond[-2:]]
		df['nsyn_elbow'] = [nsyn]
		df['nsyn_3'] = [nsyn3]
		df['nsyn_95'] = [nsyn95]
		df['nsyn_95_3'] = [nsyn95_3]
		lista.append(df)
nsyn = pd.concat(lista,ignore_index=True)
nsyn.to_pickle('./nsynn.pickle')

fig,ax = plt.subplots(3,2,figsize=(15,15),sharey=False)
ax = ax.ravel()

sns.lineplot(x='vel',y='nsyn_elbow',data=nsyn,hue='gait',ci='sd',ax=ax[0])
sns.lineplot(x='vel',y='nsyn_3',data=nsyn,hue='gait',ci='sd',ax=ax[1])
sns.lineplot(x='vel',y='nsyn_95',data=nsyn,hue='gait',ci='sd',ax=ax[2])
sns.lineplot(x='vel',y='nsyn_95_3',data=nsyn,hue='gait',ci='sd',ax=ax[3])
sns.lineplot(x='rango',y='evar',data=VAF,hue='condition',ci=95,ax=ax[5],err_style='bars')
ax[5].plot([1,7],[.9,.9],'--k')


plt.show()

# mean_squared_error(y_true, y_pred)
