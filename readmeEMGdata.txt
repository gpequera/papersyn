EMG readme

Estructura de datos de EMG para input de NMF.

La estructura consiste en un diccionario.
Cada keyword representa un experimento.
El keyword tiene 11 caractéres que definen cada experimento.
ejemplo: 'GP_R_110_ld'

Los dos primeros caracteres representan el sujeto medido (GP)
El cuarto caracter, el gait (R)
El sexto, séptimo y octavo; la velocidad (110 --> 11 km/h)
Los últimos dos, el leg (ld --> leading)

Para running y walking solo tenemos el trailing, para skipping leading y trailing


Para levantar el diccionario:
emg = np.load('./emg.npy',allow_pickle=True).item()
