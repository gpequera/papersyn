import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import pingouin as pg

# vaf = pd.read_pickle('VAF.pickle')
nsyn = pd.read_pickle('./nsynn.pickle')
nsyn['velmsec'] = (nsyn.vel/3.6).round(2)
nsyn = nsyn[nsyn.velmsec==1.81]
# vaf['evar100'] = (vaf.evar)*100


fig,ax = plt.subplots(1,figsize=(4,3))

colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]
order=['Wtr','Rtr','Str','Sld']
xticks = ['Walking','Running','Skipping\n trailing','Skipping\n leading' ]

sns.despine()

sns.barplot(x='gait',y='nsyn_elbow',data=nsyn,ci='sd',
						ax=ax,palette=colorsLegs,linewidth=2,
                 		errcolor=".2",order=order, edgecolor=".2",capsize = 0.1,errwidth=.5)

ax.set_ylim([0,5])
ax.set_ylabel('Number of synergies')
ax.set_xlabel(None)
ax.set_xticklabels(xticks)

fig.savefig('./NewSubmissionJune2021/fig2.pdf', bbox_inches='tight')
# plt.show()

kruskal = pg.kruskal(data=nsyn,dv='nsyn_elbow',between='gait')
anova = pg.anova(data=nsyn,dv='nsyn_elbow',between='gait')
