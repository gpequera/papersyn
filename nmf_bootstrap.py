#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# core Python modules
#
import pickle   # object serializatin
import argparse # parse command line
import re       # regular expressions
import pandas as pd
#
# third party modules (available through PIP and other managers)
#
import numpy as np # matrices
import matplotlib.pyplot as plt
import pandas      # data
import nimfa       # NMF
import sklearn.cluster     # k-means
#
# our modules
#

#
# main code
#
if __name__ == '__main__':
    epilog = "Non Negative Matrix Factorization of muscle EMG signals."
    parser = argparse.ArgumentParser(epilog=epilog)
    parser.add_argument("--bsamples", type=int, default="10",
                        help="Number of bootstrap samples")
    parser.add_argument("--bsize", type=int, default=5,
                        help="Bootstrap sample size")
    parser.add_argument("--min-change", type=float, default=1e-3,
                        help="minimum relative change between NMF iterations")
    parser.add_argument("--max-iter", type=int, default="500", 
                        help="Max. number of NMF iterations.")
    parser.add_argument("gait",  help="(R)un, (W)alk or (S)kip")
    parser.add_argument("leg",   help="lead (ld) or trailing (tr)")
    parser.add_argument("speed", help="speed (in hectometers per hour, zero padded to 3 digits, e.g., 5km/h -> 050)")
    args = parser.parse_args()

    #
    # load experimental data
    # it is stored as a Python dictionary whose
    # keys are the experiment codenames and the
    # objects are Panda DataFrames
    #
    f = open('emg.pickle','rb')
    data_dict = pickle.load(f)
    f.close()
    #
    # Select all samples for a given gait, speed and leg
    #
    # the keys have the form PP_G_SSS_LL
    # where
    # PP are the initials of the person
    # G is the gait: 'R' for run, 'S' for skipping, 'W' for walk
    # SSS is the speed in hm/h (10 hm = 1km)
    # LL indicates the leg: 'ld' for lead, 'tr' for trailing
    #
    rank_95    = list()
    #
    # filtramos experimentos que cumplan requisitos
    #
    expr  = r'\w{2}_' + args.gait + '_' + args.speed + '_' + args.leg
    cre   = re.compile(expr) 
    matching_keys = [k for k in data_dict.keys() if cre.match(k)]
    if len(matching_keys) == 0:
        print('Error: no matching experiments')
        exit(1)
    
    lista = []
    #
    # performn boostrap samples
    # 
    all_loads = np.zeros((7*args.bsamples,7))
    for n,i in enumerate(range(args.bsamples)):
        #
        # select subsample
        #
        print('subsample...')
        subsample_keys = np.random.choice(matching_keys,args.bsize)
        #
        # join the matrices
        # 
        print('assemble...')
        samples = list([data_dict.get(k).to_numpy() for k in subsample_keys])
        X = np.concatenate(samples,axis=0)
        print(X.shape)
        nmf = nimfa.Nmf(X.T, max_iter=500, min_residuals=1e-3, update='euclidean', objective='fro', n_run=10,rank=7)
        if True:
            #
            # analyze FAV according to rank
            #
            print('estimate rank...')
            rank_est = nmf.estimate_rank(rank_range=np.arange(1,8),n_run=10,what=('rss','evar'))
            found = False
            for r in np.arange(1,8,dtype=int):
                r_stats = rank_est[r]
                rango = pd.DataFrame()
                cond = [matching_keys[0][3:]]
                rango['condition'] = cond
                rango['rango'] = [r]
                rango['subsample'] = [n]
                print(r_stats,end=' ')
                evar = r_stats['evar']
                rango['evar'] = [evar]
                lista.append(rango)

                if not found and evar > 0.95:
                    rank_95.append(evar)
                    found = True
                    print('*')
                else:
                    print()
        else:
            #
            # compute all modes, store them for clustering
            #
            fit_nmf = nmf.factorize()
            loads = np.array(fit_nmf.basis())
            all_loads[7*i:7*(i+1),:] = loads
    for nclusters in np.arange(1,8,dtype=int):
        kmeans = sklearn.cluster.KMeans(n_clusters=nclusters).fit(all_loads)
        print('Clusters',nclusters,'Score ',kmeans.score(all_loads))
    print('finished')

    df = pd.concat(lista,ignore_index=True)
    df.to_pickle('./'+cond[0]+'.pickle')
