#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
import seaborn as sns
from pingouin import ttest
from itertools import combinations


allcosim = pd.read_pickle("./cosim")
cosim =  allcosim[allcosim.vel.isin([6.5])]
cosim['velmsec'] = np.round(cosim.vel/3.6,2)


listdfa = []
listdfb = []
for comparacion in list(cosim.comp.unique()):
    tt = cosim[cosim.comp.isin([comparacion])]
    for v in list(tt.velmsec.unique()):
        ttt = tt[tt.velmsec.isin([v])]
        for col in list(ttt.syn.unique()):
            dfa = pd.DataFrame()
            dfb = pd.DataFrame()
            
            tabla = ttt[ttt.syn.isin([col])]
            
            a = ttest(tabla['cosim'],.7,tail='greater')['p-val'].round(3).item()
            dfa['pval'] = [a]
            dfa['syn'] = col
            dfa['comp'] = comparacion
            dfa['velmsec'] = [v]
            listdfa.append(dfa)
            
            b = ttest(tabla['cosim'],.6,tail='greater')['p-val'].round(3).item()
            dfb['pval'] = [b]
            dfb['syn'] = col
            dfb['comp'] = comparacion
            dfb['velmsec'] = [v]
            listdfb.append(dfb)
            
DF1 = pd.concat(listdfa,ignore_index=True)
DF2 = pd.concat(listdfb,ignore_index=True)


# In[16]:
threshold = 'low'

if threshold == 'high':
    significance = DF1[DF1.pval>.05]
else:
    significance = DF2[DF2.pval>.05]

sns.set_theme()
sns.set(style="ticks", font_scale=.6)

conditions = ['Slead_vs_Wtr','Rtr_vs_Slead','Rtr_vs_Wtr','Str_vs_Wtr','Rtr_vs_Str','Slead_vs_Str']
titles = ['Skipping leading vs Walking','Running vs Skipping leading','Running vs Walking',
        'Skipping trailing vs Walking','Running vs Skipping trailing', 'Skipping leading vs Skipping trailing']
splot = sns.catplot(x='syn',y='cosim',col='comp',col_wrap=3,
                    data=cosim[cosim.comp.isin(conditions)],col_order=conditions,kind='bar',palette="crest",aspect=.7)
splot.set(ylim=(0, 1), yticks=[.0, .5, 1], )
splot.fig.set_size_inches((7,4))
for ax, title in zip(splot.axes.flat,titles):
    ax.set_title(title)
   
for i, cond in zip(range(len(conditions)),conditions):
    asterisco = significance[significance.comp.isin([cond])]
    for index, row in asterisco.iterrows():
        x1 = float(row.syn[1])-1
        splot.axes[i].annotate('*', 
                       (x1,1), 
                       ha = 'center', va = 'center', 
                       size=15,
                       xytext = (0, -12), 
                       textcoords = 'offset points')

splot.fig.tight_layout()
splot.axes[0].set_ylabel('cosim',fontsize=8)
splot.axes[3].set_ylabel('cosim',fontsize=8)
splot.axes[3].set_xlabel(None)
splot.axes[4].set_xlabel(None)
splot.axes[5].set_xlabel(None)

splot.savefig('./NewSubmissionJune2021/fig4.pdf', format='pdf',bbox_inches='tight')
plt.close()


# In[6]: