setwd('/home/german/Documents/repo/papersyn/')
library(coin)# Non-parametric
library(car) # for leveneTest


df = read.csv('./mech')
df$velmsec = as.factor(df$velmsec)
recovery5 = df[df$vel == "5",]
recovery65 = df[df$vel == "6.5",]
recovery9 = df[df$vel == "9",]
m5 = aov(recovery ~ gait, data=recovery5)
m65 = aov(recovery ~ gait, data=recovery65)
m9 = aov(recovery ~ gait, data=recovery9)
print(shapiro.test(residuals(m5)))
print(shapiro.test(recovery5[recovery5$gait=='W',]$recovery))
print(shapiro.test(recovery5[recovery5$gait=='S',]$recovery))

print(shapiro.test(residuals(m65)))
print(shapiro.test(recovery65[recovery65$gait=='W',]$recovery))
print(shapiro.test(recovery65[recovery65$gait=='S',]$recovery))
print(shapiro.test(recovery65[recovery65$gait=='R',]$recovery))

print(shapiro.test(residuals(m9)))
print(shapiro.test(recovery9[recovery9$gait=='R',]$recovery))
print(shapiro.test(recovery9[recovery9$gait=='S',]$recovery))

# qqnorm(residuals(m)); qqline(residuals(m))


print(leveneTest(recovery ~ gait, data=recovery5, center=mean))
print(leveneTest(recovery ~ gait, data=recovery65, center=mean))
print(leveneTest(recovery ~ gait, data=recovery9, center=mean))


recWS5 = t.test(recovery5[recovery5$gait=='W',]$recovery,                #distribucion normal de todas las variables, igual varianza
	  recovery5[recovery5$gait=='S',]$recovery,
	  mu          = 0,
	  var.equal   = TRUE,
	  conf.level  = 0.95)
p.adjust(c(recWS5$p.value), method="bonf")



recWS65 = t.test(recovery65[recovery65$gait=='W',]$recovery,				#distribucion normal de todas las variables, con homogeneidad de varianza
	  recovery65[recovery65$gait=='S',]$recovery,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)
recRS65 = t.test(recovery65[recovery65$gait=='R',]$recovery,				#distribucion normal de todas las variables, sin homogeneidad de varianza
	  recovery65[recovery65$gait=='S',]$recovery,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)
recWR65 = t.test(recovery65[recovery65$gait=='W',]$recovery,				#distribucion normal de todas las variables, sin homogeneidad de varianza
	  recovery65[recovery65$gait=='R',]$recovery,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)

print(p.adjust(c(recWS65$p.value, recRS65$p.value, recWR65$p.value), method="bonf"))

print(wilcox.test(recovery~gait, data= recovery9,conf.level = 0.95))	#no hay distribución normal ni homogeneidad de varianza



# #-----------------------------------------------------------------------------
W = read.csv('./W.csv')
W$vel = as.factor(W$vel)
W5 = W[W$vel == "1.39",]
W65 = W[W$vel == "1.81",]
W9 = W[W$vel == "2.5",]

wintM5 = aov(wext ~ gait, data=W5)
wintM65 = aov(wext ~ gait, data=W65)
wintM9 = aov(wext ~ gait, data=W9)

print(shapiro.test(residuals(wintM5)))
print(shapiro.test(W5[W5$gait=='W',]$wext))
print(shapiro.test(W5[W5$gait=='S',]$wext))

print(summary(wintM65))
print(shapiro.test(residuals(wintM65)))
print(shapiro.test(W65[W65$gait=='W',]$wext))
print(shapiro.test(W65[W65$gait=='S',]$wext))
print(shapiro.test(W65[W65$gait=='R',]$wext))

print(shapiro.test(residuals(wintM9)))
print(shapiro.test(W9[W9$gait=='R',]$wext))
print(shapiro.test(W9[W9$gait=='S',]$wext))

print(leveneTest(wext ~ gait, data=W5, center=mean))
print(leveneTest(wext ~ gait, data=W65, center=mean))
print(leveneTest(wext ~ gait, data=W9, center=mean))


print(wilcox.test(wext~gait, data= W5,conf.level = 0.95))	#no hay distribución normal


WWS65 = t.test(W65[W65$gait=='W',]$wext,				#distribucion normal de todas las variables, sin homogeneidad de varianza
	  W65[W65$gait=='S',]$wext,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)
WRS65 = t.test(W65[W65$gait=='R',]$wext,				#distribucion normal de todas las variables, sin homogeneidad de varianza
	  W65[W65$gait=='S',]$wext,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)
WWR65 = t.test(W65[W65$gait=='W',]$wext,				#distribucion normal de todas las variables, sin homogeneidad de varianza
	  W65[W65$gait=='R',]$wext,
	  mu          = 0,
	  var.equal   = FALSE,
	  conf.level  = 0.95)
print(p.adjust(c(WWS65$p.value, WRS65$p.value, WWR65$p.value)), method="bonf")
 

print(t.test(W9[W9$gait=='S',]$wext,				#distribucion normal de todas las variables, con homogeneidad de varianza
	  W9[W9$gait=='R',]$wext,
	  mu          = 0,
	  var.equal   = TRUE,
	  conf.level  = 0.95))
# library(multcomp) # for glht, mcp
# print(summary(glht(M, mcp(gait="Tukey")), test=adjusted(type="holm"))) # m is from aov
# # library(emmeans)
# # print(summary(glht(M, emm(pairwise ~ gait)), test=adjusted(type="holm")))



# library(car) # for leveneTest
# leveneTest(wext ~ gait*vel, data=W, center=mean)


# # library(ez)

# wext5 = wilcox.test(W5[W5$gait == "W",]$wext, W5[W5$gait == "S",]$wext) # a vs. b
# print(p.adjust(c(wext5$p.value), method="bonf"))

# print(kruskal_test(wext ~ gait, data=W65, distribution="asymptotic"))

# wext65ws = (wilcox.test(W65[W65$gait == "W",]$wext, W65[W65$gait == "S",]$wext)) # a vs. b
# wext65rs = (wilcox.test(W65[W65$gait == "R",]$wext, W65[W65$gait == "S",]$wext)) # a vs. b
# wext65rw = (wilcox.test(W65[W65$gait == "R",]$wext, W65[W65$gait == "W",]$wext)) # a vs. b
# print(p.adjust(c(wext65ws$p.value,wext65rs$p.value,wext65rw$p.value), method="bonf"))

# wext9 = (wilcox.test(W9[W9$gait == "R",]$wext, W9[W9$gait == "S",]$wext)) # a vs. b
# print(p.adjust(c(wext9$p.value), method="bonf"))

