import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols
import numpy as np

vafN = pd.read_pickle('./nsynSeaborn')
vafN['velmsec'] = np.round(vafN.vel/3.6,2)
a= vafN.gait.to_list()
for n, i in enumerate(a):
	if i == 'S*lead':
		a[n] = 'Slead'
	if i == 'S*tr':
		a[n] = 'Str'

vafN['gait'] = a

vafN5 = vafN[vafN.vel==5]
vafN65 = vafN[vafN.vel==6.5]
vafN9 = vafN[vafN.vel==9]

vafNStr = vafN[vafN.gait=='Str']
vafNSld = vafN[vafN.gait=='Slead']
vafNR = vafN[vafN.gait=='R']
vafNW = vafN[vafN.gait=='W']

for data,v in zip([vafN5, vafN65, vafN9],[5, 6.5, 9, 'Str','Sld', 'R', 'W']):
	norm = pg.normality(data,group='gait', dv='nsyn').round(3)
	norm['var'] = ['nsyn']*norm.shape[0]
	norm['vel'] = [v]*norm.shape[0]
	# print(norm)
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)



for data,v in zip([vafNStr, vafNSld, vafNR, vafNW],['Str','Sld', 'R', 'W']):
	norm = pg.normality(data,group='velmsec', dv='nsyn').round(3)
	norm['var'] = ['nsyn']*norm.shape[0]
	norm['vel'] = [v]*norm.shape[0]
	# print(norm)
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)


print(pg.kruskal(data=vafN, dv='nsyn', between='gait'))
print(pg.pairwise_ttests(dv='nsyn', between='gait', data=vafN, padjust='bonf',parametric=False).round(3))
print(pg.kruskal(data=vafN, dv='nsyn', between='velmsec'))
print(pg.pairwise_ttests(dv='nsyn', between='velmsec', data=vafN, padjust='bonf',parametric=False).round(3))
# for data in [vafN5, vafN65, vafN9]:
# 	print(pg.kruskal(data=data, dv='nsyn', between='gait'))

# for data in [vafNStr, vafNSld, vafNR, vafNW]:
# 	print(pg.kruskal(data=data, dv='nsyn', between='velmsec'))	