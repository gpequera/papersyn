import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np



cot = pd.read_pickle('./CoT')
cot['velmsec'] = np.round(cot.vel/3.6,2)
energetic = pd.read_pickle('./energetic')
mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
# Wint = pd.read_pickle('./wint')
mech['velmsec'] = np.round(mech.vel/3.6,2)
W = pd.read_pickle('./W')
eff = pd.DataFrame()
cotmean = cot.groupby(['vel', 'gait']).mean().reset_index()
Wmean = W.groupby(['vel', 'gait']).mean().reset_index()
eff['eff'] = Wmean.wtotal/cotmean.COT
eff['gait'] = cotmean.gait 
eff['vel'] = cotmean.velmsec 

# mech_regressions = pd.read_pickle('./mech_regressions.pickle')
mech_regressions = pd.read_pickle('./reg_spt.pickle')


colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

sns.set(style="white", font_scale=1)
err_kws = {'elinewidth':1,'capsize':3}

fig1,ax1 = plt.subplots(4,2,figsize=(8,10))
plt.subplots_adjust(wspace=.4)
g1 = sns.scatterplot(x='velmsec',y='COT',data=cot,hue='gait', hue_order=['W','R','S'], ax=ax1[0,0],palette= colors, s=10)
walking = mech_regressions[mech_regressions.gait.isin(['W'])]
running = mech_regressions[mech_regressions.gait.isin(['R'])]
skipping = mech_regressions[mech_regressions.gait.isin(['S'])]

# sns.lineplot(x='velmsec',y='values2',data=walking[walking.variable.isin(['COT'])],hue='gait', hue_order=['W','R','S'], ax=ax1[0,0],palette= colors)
sns.lineplot(x='velmsec',y='values2',data=walking[walking.variable.isin(['COT'])], ax=ax1[0,0],color= colors[0],ci=95)
sns.lineplot(x='velmsec',y='values1',data=running[running.variable.isin(['COT'])], ax=ax1[0,0],color= colors[1],ci=95)
sns.lineplot(x='velmsec',y='values2',data=skipping[skipping.variable.isin(['COT'])], ax=ax1[0,0],color= colors[2],ci=95)

g1.set_ylabel('COT (J/kg/m)')
g1.set(xlabel=None)
g1.set(xticklabels=[])
g1.legend_.remove()
# sns.set()
sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()


# sns.set_style("ticks")
# sns.set_context("paper",font_scale = 1.3)
# sns.despine()


g2 = sns.scatterplot(x='velmsec',y='pot',data=cot,hue='gait', hue_order=['W','R','S'], ax=ax1[0,1],palette= colors, s=10)
# sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['pot'])],hue='gait', hue_order=['W','R','S'], ax=ax1[0,1],palette= colors,ci=95)
sns.lineplot(x='velmsec',y='values2',data=walking[walking.variable.isin(['pot'])], ax=ax1[0,1],color= colors[0],ci=95)
sns.lineplot(x='velmsec',y='values1',data=running[running.variable.isin(['pot'])], ax=ax1[0,1],color= colors[1],ci=95)
sns.lineplot(x='velmsec',y='values1',data=skipping[skipping.variable.isin(['pot'])], ax=ax1[0,1],color= colors[2],ci=95)
g2.legend_.remove()
g2.set_ylabel('Metabolic power (W/kg)')
sns.set_style("ticks")
sns.set_context("paper",font_scale = 1.3)
sns.despine()
g2.set(xlabel=None)
g2.set(xticklabels=[])


g3 = sns.scatterplot(x='velmsec',y='recovery',hue='gait', hue_order=['W','R','S'], data=mech, ax= ax1[1,0],palette=colors, s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['recovery'])],hue='gait', hue_order=['W','R','S'], ax=ax1[1,0],palette= colors,ci=95)
g3.legend_.remove()
g3.set(xlabel=None)
# g3.set(xticklabels=[])

ax1[3,1].axis('off') 

g4 = sns.scatterplot(x='vel',y='wext',hue='gait', hue_order=['W','R','S'], data=W, ax= ax1[1,1], palette=colors, s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['wext'])],hue='gait', hue_order=['W','R','S'], ax=ax1[1,1],palette= colors,ci=95)
g4.legend_.remove()
g5 = sns.scatterplot(x='vel',y='wint',hue='gait', hue_order = ['W','R','S'], data=W, ax= ax1[2,0], palette=colors, s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['wint'])],hue='gait', hue_order=['W','R','S'], ax=ax1[2,0],palette= colors,ci=95)
g5.legend_.remove()
g6 = sns.scatterplot(x='vel',y='wtotal',hue='gait', hue_order = ['W','R','S'], data=W, ax= ax1[2,1], palette=colors, s=10)
sns.lineplot(x='velmsec',y='values2',data=mech_regressions[mech_regressions.variable.isin(['wtotal'])],hue='gait', hue_order=['W','R','S'], ax=ax1[2,1],palette= colors,ci=95)
g6.legend_.remove()
# g7 = sns.lineplot(x='vel',y='eff',hue_order = ['W','R','S'],data=eff,hue='gait',ax= ax1[3,0], palette=colors, dashes=False,markers= ['o','o','o'], style="gait")
g7 = sns.lineplot(x='vel',y='eff',hue_order = ['W','R','S'],data=eff,hue='gait',ax= ax1[3,0], palette=colors, linestyle='',marker='o', markersize=6)
leg_handles = g7.get_legend_handles_labels()[0]
g7.legend(leg_handles,['Walking','Running','Skipping'],frameon=False,loc='center left', bbox_to_anchor=(1.1, .45))

g3.set_ylabel('Recovery (%)')
g4.set_ylabel('Wext (J/Kg.m)')
# g4.set_xlabel('Speed (m.sec$^{-1}$)')
g5.set_ylabel('Wint (J/Kg.m)')
g5.set_xlabel('Speed (m.sec$^{-1}$)')
g6.set_xlabel('Speed (m.sec$^{-1}$)')
g6.set_ylabel('Wtotal (J/Kg.m)')
g7.set_ylabel('Efficency')
g7.set_xlabel('Speed (m.sec$^{-1}$)')
# g7.set_yticks([.1,.2,.3,.4,.5])
# g7.set_yticklabels([.1,.2,.3,.4,.5])

ejex = np.round(np.sort(W.vel.unique()),1)

g3.set(xlabel=None)
g3.set(xticklabels=[])
g4.set(xlabel=None)
g4.set(xticklabels=[])
g5.set(xlabel=None)
g5.set(xticklabels=[])
g6.set_xticks(ejex)
g6.set_xticklabels(ejex)
g7.set_xticks(ejex)
g7.set_xticklabels(ejex)
g1.text(.8,6,'A',fontsize=16)
g2.text(.8,13,'B',fontsize=16)

g3.text(.8,75,'C',fontsize=16)
g3.text(1.39,75,'*',fontsize=10)
g3.text(1.77,75,'* # $\dag$',fontsize=10)
g3.text(2.5,75,' $\dag$ ',fontsize=10)	

g4.text(.8,2.3,'D',fontsize=16)
g4.text(1.39,2.3,'*',fontsize=10)
g4.text(1.76,2.3,'* # $\dag$',fontsize=10)
g4.text(2.5,2.3,r'$\dag$',fontsize=10)

g5.text(.8,.68,'E',fontsize=16)
g5.text(1.39,.68, '*',fontsize=12)
g5.text(1.81,.68, '* $\dag$',fontsize=10)
g5.text(2.5,.68, '$\dag$',fontsize=10)


g6.text(.8,2.8,'F',fontsize=16)
g6.text(1.39,2.8, '*',fontsize=10)
g6.text(1.8,2.8,'* # $\dag$',fontsize=10)
g6.text(2.5,2.8, '$\dag$',fontsize=10)

g7.text(1,.49,'G',fontsize=16)


plt.savefig('./fig1RE.pdf')
plt.savefig('./fig1RE.png')
# plt.close()



