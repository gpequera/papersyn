import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

colors = ["#D95F02","#1B9E77",'#000000']
colorsLegs = ["#D95F02","#1B9E77",'#E7298A',"#7570B3"]

vafN = pd.read_pickle('./nsynSeaborn')
vafN['velmsec'] = np.round(vafN.vel/3.6,2)
a= vafN.gait.to_list()
for n, i in enumerate(a):
	if i == 'S*lead':
		a[n] = 'Slead'
	if i == 'S*tr':
		a[n] = 'Str'

vafN['gait'] = a

sns.set_theme()
sns.set_context("paper")
sns.set_style("white")
g2 = sns.displot(vafN, x="nsyn", col="velmsec", hue='gait', hue_order=['W','R','Str','Slead'],multiple="dodge",
facet_kws=dict(margin_titles=False), palette=colorsLegs, legend=True,
)
g2.fig.set_size_inches((12,3))
msec = " m.sec$^{{-1}}$"
g2.set_titles("{col_name}"+ msec)

g2.legend.set_bbox_to_anchor((.12,.75))
g2.legend.set_title(None)
# leg_handles = g2.get_legend_handles_labels()[0]
# labels = ['Walking','Running','Skipping \ntrailing','Skipping \nleading']
# plt.legend(title=None, loc='upper left', labels=labels)
# g2.legend(leg_handles,labels,frameon=False)
# g2.legend.texts = [(0, 0, 'Walking'), (0, 0, 'R'), (0, 0, 'Str'), (0, 0, 'Slead')]
g2.set(ylabel='# of subjects',xlabel='# of synergies')

# 
# sns.despine()
#

plt.tight_layout()
plt.show()
g2.savefig('./fig3RE.png')
g2.savefig('./fig3RE.pdf')