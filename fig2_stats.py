import pandas as pd
import pingouin as pg
import statsmodels.api as sm
from statsmodels.formula.api import ols
import numpy as np

mech = pd.read_csv('./mech.csv')
# dutyFactor = pd.read_pickle('./spatiotemporal')
# dutyFactor['velmsec'] = dutyFactor.vel/3.6
DFT = pd.read_pickle('./DutyFlightTime')
DFT['velmsec'] = np.round(DFT.vel/3.6,2)

dutyFactor = pd.read_pickle('./spatiotemporal')
dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
dutyLD.drop(['gait'], axis=1)
dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
DF = pd.concat([dutyTR,dutyLD],ignore_index=True)
DF['velmsec'] = np.round(DF.vel/3.6,2)

mech5 = mech[mech.vel==5]
mech65 = mech[mech.vel==6.5]
mech9 = mech[mech.vel==9]
mechS = mech[mech.gait=='S']
mechR = mech[mech.gait=='R']
mechW = mech[mech.gait=='W']

print('---------normality--------------------------')
for data,v in zip([mech5, mech65, mech9],[5, 6.5, 9]):
	for var in ['strideFreq','strideLength']:
		norm = pg.normality(data,group='gait', dv=var).round(3)
		norm['var'] = [var]*norm.shape[0]
		norm['vel'] = [v]*norm.shape[0]
		df = (norm[~norm["normal"]])
		if not df.empty:
			print(df)

for data,g in zip([mechW, mechS, mechR],['W', 'S', 'R']):
	for var in ['strideFreq','strideLength']:
		norm = pg.normality(data,group='vel', dv=var).round(3)
		norm['var'] = [var]*norm.shape[0]
		norm['gait'] = [g]*norm.shape[0]
		df = (norm[~norm["normal"]])
		if not df.empty:
			print(df)

DFT65 = DFT[DFT.vel==6.5]
DFT9 = DFT[DFT.vel==9]
DFTR = DFT[DFT.gait=='R']
DFTS = DFT[DFT.gait=='S']

for data,v in zip([DFT65, DFT9],[6.5, 9]):
	norm = pg.normality(data,group='gait', dv='dutyFlight').round(3)
	norm['var'] = ['dutyFlight']*norm.shape[0]
	norm['vel'] = [v]*norm.shape[0]
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)
for data,g in zip([DFTS, DFTR],['S', 'R']):
	norm = pg.normality(data,group='vel', dv='dutyFlight').round(3)
	norm['var'] = ['dutyFlight']*norm.shape[0]
	norm['gait'] = [v]*norm.shape[0]
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)

DF5 = DF[DF.vel==5]
DF65 = DF[DF.vel==6.5]
DF9 = DF[DF.vel==9]
DFW = DF[DF.gait=='W']
DFStr = DF[DF.gait=='S']
DFSld = DF[DF.gait=='Sld']
DFR = DF[DF.gait=='R']


for data,v in zip([DF5, DF65, DF9],[5, 6.5, 9]):
	norm = pg.normality(data,group='gait', dv='DutyFactor').round(3)
	norm['var'] = ['DutyFactor']*norm.shape[0]
	norm['vel'] = [v]*norm.shape[0]
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)
for data,g in zip([DFW,DFStr,DFSld, DFR],['W','Str', 'Sld', 'R']):
	norm = pg.normality(data,group='vel', dv='DutyFactor').round(3)
	norm['var'] = ['DutyFactor']*norm.shape[0]
	norm['gait'] = [v]*norm.shape[0]
	df = (norm[~norm["normal"]])
	if not df.empty:
		print(df)


#termina test de normalidad
#comienza Anova

for way in ['gait','vel']:
	for var in ['strideFreq','strideLength']:
		print('--------anova one way-----------------------------')
		aov = pg.anova(data=mech, dv=var, between=way, detailed=True)
		print('--------------ANOVA by ' + way + '--> '+var +' -----------')
		print(aov)

for var in ['strideFreq','strideLength']:
		print('--------anova one way 6.5---- '+ var +' -----------------------')
		aov = pg.anova(data=mech65, dv=var, between='gait', detailed=True)
		print('--------------ANOVA by gait --> '+var +' -----------')
		print(aov)
for data,g in zip([mechW, mechS, mechR],['W', 'S', 'R']):
	for var in ['strideFreq','strideLength']:
		print('--------anova one way--- '+g+' --------------------------')
		aov = pg.anova(data=data, dv=var, between=way, detailed=True)
		print('--------------ANOVA by ' + way + '--> '+var +' -----------')
		print(aov)

print('----------------pairwise------------------')

for data,v in zip([mech5, mech65, mech9],[5, 6.5, 9]):
	for var in ['strideFreq','strideLength']:
		pairwise = pg.pairwise_ttests(dv=var, between='gait', data=data, padjust='bonf',parametric=True).round(3)
		pairwise['comp'] = [var + str(data.vel.unique().item())[:2]]*pairwise.shape[0]  
		print('-------------pairwise-----by gait --> vel '+str(v) + ' ' + var+ '----------------')
		print(pairwise)	
for data,g in zip([mechW, mechS, mechR],['W', 'S', 'R']):
	for var in ['strideFreq','strideLength']:
		pairwise = pg.pairwise_ttests(dv=var, between='vel', data=data, padjust='bonf',parametric=True).round(3)
		pairwise['comp'] = [var + str(data.gait.unique().item())[:2]]*pairwise.shape[0]  
		print('-------------pairwise-----by vel --> gait '+str(g) + ' ' + var+ '----------------')
		print(pairwise)



#comienza Anova duty flight

for way in ['gait','vel']:
	print('--------kruskal dutyFlighttotal data----------------------------')
	kruskal = pg.kruskal(data=DFT, dv='dutyFlight', between=way, detailed=True)
	print('--------------kruskal by ' + way + '--> '+'dutyFlight total data----------')
	print(kruskal)

for data,g in zip([DFTS, DFTR],['S', 'R']): 
	print('--------anova one way dutyFlight by vel-----------------------------')
	kruskal = pg.kruskal(data=data, dv='dutyFlight', between='vel', detailed=True)
	print(kruskal)

print('----------------pairwise------------------')

for data,v in zip([DFT65,  DFT9],[6.5, 9]):
	pairwise = pg.pairwise_ttests(dv='dutyFlight', between='gait', data=data, padjust='bonf',parametric=False).round(3)
	pairwise['comp'] = ['dutyFlight' + str(data.vel.unique().item())[:2]]*pairwise.shape[0]  
	print('-------------pairwise-----by gait --> vel '+str(v) + ' dutyFlight----------------')
	print(pairwise)
for data,g in zip([DFTS, DFTR],['S', 'R']):
	pairwise = pg.pairwise_ttests(dv='dutyFlight', between='vel', data=data, padjust='bonf',parametric=False).round(3)
	pairwise['comp'] = ['dutyFlight' + str(data.gait.unique().item())[:2]]*pairwise.shape[0]  
	print('-------------pairwise-----by vel --> gait '+str(g) + ' dutyFlight----------------')
	print(pairwise)

#comienza kruskal duty factor
for way in ['gait','vel']:
	print('--------kruskal DutyFactor----------------------------')
	kruskal = pg.kruskal(data=DF, dv='DutyFactor', between=way, detailed=True)
	print('--------------kruskal by ' + way + '--> dutyFactor' +' -----------')
	print(kruskal)

for data,v in zip([DF5, DF65, DF9],[5, 6.5, 9]):
	print('--------anova one way DutyFactor by gait ' +str(v)+ ' --------------------------')
	kruskal = pg.kruskal(data=data, dv='DutyFactor', between='gait', detailed=True)
	print(kruskal)
for data,g in zip([DFW,DFStr,DFSld, DFR],['W','Str', 'Sld', 'R']):
	print('--------anova one way DutyFactor by vel ' +g+ ' --------------------------')
	kruskal = pg.kruskal(data=data, dv='DutyFactor', between='vel', detailed=True)
	print(kruskal)


for data,v in zip([DF5, DF65, DF9],[5,6.5,9]):
	pairwise = pg.pairwise_ttests(dv='DutyFactor', between='gait', data=data, padjust='bonf',parametric=False).round(3)
	pairwise['comp'] = ['DutyFactor' + str(data.vel.unique().item())[:2]]*pairwise.shape[0]
	print('-------------------pairwise-----by gait '+'-->'+ str(v) +'------------------')
	print(pairwise)
print('------------------------------------------------------------')

for data,g in zip([DFW,DFStr,DFSld, DFR],['W','Str', 'Sld', 'R']):
	pairwise = pg.pairwise_ttests(dv='DutyFactor', between='vel', data=data, padjust='bonf',parametric=False).round(3)
	pairwise['comp'] = ['DutyFactor' + str(data.gait.unique().item())[:2]]*pairwise.shape[0]
	print('-------------------pairwise-----by vel '+'-->'+ g +'------------------')
	print(pairwise)
# DF5 = DF[DF.vel==5]
# DF65 = DF[DF.vel==6.5]
# DF9 = DF[DF.vel==9]

# for data,v in zip([DF5, DF65, DF9],[5, 6.5, 9]):
# 	#for var in ['dutyFactor_tr','dutyFactor_ld']:
# 	norm = pg.normality(data,group='gait', dv='DutyFactor').round(3)
# 	norm['var'] = ['DutyFactor']*norm.shape[0]
# 	norm['vel'] = [v]*norm.shape[0]
# 	# print(norm)
# 	df = (norm[~norm["normal"]])
# 	if not df.empty:
# 		print(df)

# aov = pg.anova(data=DF5, dv='DutyFactor', between='gait', detailed=False)
# print(aov)

# for data in [DF65,DF9]:
# 	aov = pg.kruskal(data=data, dv='DutyFactor', between='gait', detailed=False)
# 	print(aov)

# print('---------------------------------------------------------------------------------------------')

# for data in [mech5, mech65, mech9]:
# 	for var in ['strideFreq','strideLength']:
# 		pairwise = pg.pairwise_ttests(dv=var, between='gait', data=data, padjust='bonf',parametric=True).round(3)
# 		pairwise['comp'] = [var + str(data.vel.unique().item())[:2]]*pairwise.shape[0]  
# 		print(pairwise)


# for data,parametric in zip([DFT65, DFT9],[True, False]):
# 		pairwise = pg.pairwise_ttests(dv='dutyFlight', between='gait', data=data, padjust='bonf',parametric=parametric).round(3)
# 		pairwise['comp'] = ['dutyFlight' + str(data.vel.unique().item())[:2]]*pairwise.shape[0]
# 		print(pairwise)


# for data,parametric in zip([DF5, DF65, DF9],[True, False, False]):
# 		pairwise = pg.pairwise_ttests(dv='DutyFactor', between='gait', data=data, padjust='bonf',parametric=parametric).round(3)
# 		pairwise['comp'] = ['DutyFactor' + str(data.vel.unique().item())[:2]]*pairwise.shape[0]
# 		print(pairwise)
# print('------------------------------------------------------------')