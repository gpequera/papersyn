#!/usr/bin/env python
# coding: utf-8

# In[8]:


import pandas as pd
# import numpy as np
# import seaborn as sns
# import matplotlib.pyplot as plt


lista = ['R_065_tr', 'R_090_tr', 'R_110_tr', 'S_050_tr', 'S_065_tr', 'S_090_tr', 'S_050_ld', 'S_065_ld', 'S_090_ld', 'W_030_tr', 'W_040_tr', 'W_050_tr', 'W_065_tr']

dflista = []
for cond in lista:
    dflista.append(pd.read_pickle('./'+cond+'.pickle'))
VAF = pd.concat(dflista,ignore_index=True)
VAF.to_pickle('./VAF.pickle')


# sns.lineplot(x='rango',y='evar',data=VAF,hue='condition')

