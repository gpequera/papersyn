import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score


cot = pd.read_pickle('./CoT')
cot['velmsec'] = np.round(cot.vel/3.6,2)
energetic = pd.read_pickle('./energetic')
mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
# Wint = pd.read_pickle('./wint')
mech['velmsec'] = np.round(mech.vel/3.6,2)
W = pd.read_pickle('./W')
W['velmsec'] = W.vel.values

dutyFactor = pd.read_pickle('./spatiotemporal')
dutyLD = dutyFactor[dutyFactor.gait.isin(['S'])][['subject','gait','vel','dutyFactor_ld']]
dutyLD.drop(['gait'], axis=1)
dutyLD['gait'] = ['Sld']*dutyLD.shape[0]
dutyLD.rename(columns={'dutyFactor_ld': 'DutyFactor'}, inplace=True)
dutyTR = dutyFactor[['subject','gait','vel','dutyFactor_tr']]
dutyTR.rename(columns={'dutyFactor_tr': 'DutyFactor'}, inplace=True)     
dutyfactor = pd.concat([dutyTR,dutyLD],ignore_index=True)
dutyfactor['velmsec'] = np.round(dutyfactor.vel/3.6,2)

DFT = pd.read_pickle('./DutyFlightTime')
DFT['velmsec'] = (DFT.vel/3.6).round(2)


del dutyFactor, dutyTR, dutyLD

N = 100
lista = []
lista1 = []
listaduty = []
listaft = []

for name, data in zip(['cot','mech', 'W'],[cot, mech, W]):
	for gait in data.gait.unique():
		subdata = data[data.gait.isin([gait])]
		for vel in subdata.velmsec.unique():
			subdataMech = subdata[subdata.velmsec.isin([vel])]
			if name == 'cot':
				variables = ['COT', 'pot']
			elif name == 'mech':
				variables = ['recovery', 'strideFreq', 'strideLength']
			else:
				variables = ['wint', 'wext', 'wtotal']
			for var in variables:
				df = pd.DataFrame()
				sMech = subdataMech.reset_index(drop=True)[var]
				ind = np.random.choice(sMech.index.values,N)
				df['values'] = sMech.loc[ind].values
				df['var'] = [var]*N
				df['velmsec'] = [vel]*N
				df['gait'] = [gait]*N
				df['N'] = np.arange(N)
				lista.append(df)

bootstrap = pd.concat(lista,ignore_index=True)
bootstrap.to_pickle('./bootstraped_spt.pickle')
# varfig = sns.catplot(x='velmsec',y='values',hue='gait',data=bootstrap,col='var' ,col_wrap=4,kind='point',sharey=False)


for name,data in zip(['DutyFactor', 'dutyFlight'],[dutyfactor, DFT]):
	for gait in data.gait.unique():
		subdata = data[data.gait.isin([gait])]
		for vel in subdata.velmsec.unique():
			# if name == 'duty':
			# 	variable = 'DutyFactor'
			# else:
			# 	variable = 'dutyFlight'

			subset = subdata[subdata.velmsec.isin([vel])][name]
			s = subset.reset_index(drop=True)
			ind = np.random.choice(s.index.values,N)
			df = pd.DataFrame()
			df[name] = s.loc[ind]
			df['velmsec'] = [vel]*N
			df['gait'] = [gait]*N
			df['N'] = np.arange(N)
			if name == 'DutyFactor':
				listaduty.append(df)
			else:
				listaft.append(df)

bootstrapDuty = pd.concat(listaduty,ignore_index=True)
bootstrapDuty.to_pickle('./bootstraped_Duty.pickle')
bootstrapFT = pd.concat(listaft,ignore_index=True)
bootstrapFT.to_pickle('./bootstraped_FT.pickle')



lista_reg = []
for gait in bootstrap.gait.unique():
	forGait = bootstrap[bootstrap.gait.isin([gait])]
	for n in range(N):
		forN = forGait[forGait.N.isin([n])]
		for var in ['recovery', 'strideFreq', 'strideLength', 'wint', 'wext', 'wtotal','COT','pot']:
			toreg =  forN[forN['var'].isin([var])].sort_values(by='velmsec')
			x = toreg.velmsec.values
			y = toreg['values']
			coeff2 = np.polyfit(x,y,2)
			y_pred2 = coeff2[0]*x**2+coeff2[1]*x+coeff2[2]
			r2_2 = r2_score(y,y_pred2)
			coeff1 = np.polyfit(x,y,1)
			y_pred1 = coeff1[0]*x+coeff1[1]
			r2_2 = r2_score(y,y_pred2)
			r2_1 = r2_score(y,y_pred1)
			df = pd.DataFrame()
			df['aa'] = [coeff2[0]]
			df['bb'] = [coeff2[1]]
			df['cc'] = [coeff2[2]]
			df['a'] = [coeff1[0]]
			df['b'] = [coeff1[1]]
			df['r_1_square'] = [r2_1.round(3)]
			df['r_2_square'] = [r2_2.round(3)]
			df['gait'] = [gait]
			df['var'] = [var]
			df['N'] = [n]
			l = x.shape[0]
			df_reg = pd.DataFrame()
			df_reg['velmsec'] = x
			df_reg['values2'] = y_pred2
			df_reg['values1'] = y_pred1
			df_reg['gait'] = [gait]*l
			df_reg['N'] = [n]*l
			df_reg['variable'] = [var]*l
			lista1.append(df)
			lista_reg.append(df_reg)

coeff_spt = pd.concat(lista1,ignore_index=True)
coeff_spt.to_pickle('./coeff_spt.pickle')
reg_spt = pd.concat(lista_reg,ignore_index=True)
reg_spt.to_pickle('./reg_spt.pickle')

listaDuty= []
lista_regDuty = []

listaFT= []
lista_regFT = []



for name,data in zip(['DutyFactor','dutyFlight'],[bootstrapDuty,bootstrapFT]):
	for gait in data.gait.unique():
		for n in range(N):
			toreg = data[data.gait.isin([gait]) & data.N.isin([n])].sort_values(by='velmsec')
			x = toreg.velmsec.values
			y = toreg[name].values
			coeff2 = np.polyfit(x,y,2)
			y_pred2 = coeff2[0]*x**2+coeff2[1]*x+coeff2[2]
			r2_2 = r2_score(y,y_pred2)
			coeff1 = np.polyfit(x,y,1)
			y_pred1 = coeff1[0]*x+coeff1[1]
			r2_1 = r2_score(y,y_pred1)
			df = pd.DataFrame()
			df['aa'] = [coeff2[0]]
			df['bb'] = [coeff2[1]]
			df['cc'] = [coeff2[2]]
			df['a'] = [coeff1[0]]
			df['b'] = [coeff1[1]]
			df['r_1_square'] = [r2_1.round(3)]
			df['r_2_square'] = [r2_2.round(3)]
			df['gait'] = [gait]
			df['var'] = [name]
			df['N'] = [n]
			l = x.shape[0]
			df_reg = pd.DataFrame()
			df_reg['velmsec'] = x
			df_reg['values2'] = y_pred2
			df_reg['values1'] = y_pred1
			df_reg['gait'] = [gait]*l
			df_reg['N'] = [n]*l
			df_reg['variable'] = [name]*l
			if name == 'DutyFactor':
				lista_regDuty.append(df_reg)
				listaDuty.append(df)
			else:
				lista_regFT.append(df_reg)
				listaFT.append(df)

coeff_duty = pd.concat(listaDuty,ignore_index=True)
coeff_duty.to_pickle('./coeff_duty.pickle')
reg_duty = pd.concat(lista_regDuty,ignore_index=True)
reg_duty.to_pickle('./reg_duty.pickle')

coeff_ft = pd.concat(listaFT,ignore_index=True)
coeff_ft.to_pickle('./coeff_ft.pickle')
reg_ft = pd.concat(lista_regFT,ignore_index=True)
reg_ft.to_pickle('./reg_ft.pickle')


			
