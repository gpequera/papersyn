import pandas as pd
import numpy as np

cot = pd.read_pickle('./CoT')
energetic = pd.read_pickle('./energetic').sort_values(by=['subject','gait','vel']).reset_index()
# mech = energetic.reset_index().sort_values(by=['subject','gait','vel'])
Wint = pd.read_pickle('/home/german/ownCloud/work/phd/proyect_2016/data/energetic/wint').sort_values(by=['subject','gait','vel']).reset_index()
energetic['velmsec'] = np.round(energetic.vel/3.6,2)
borrar = energetic[energetic.gait.isin(['R']) & energetic.subject.isin(['GP'])  & energetic.velmsec.isin([1.81])].index
energetic = energetic.drop(borrar.values).sort_values(by=['subject','gait','vel']).reset_index()

W = pd.DataFrame()
W['wint'] = Wint.Wint
W['wext'] = energetic.Wext
W['wtotal'] = Wint.Wint + energetic.Wext
W['vel'] = energetic.velmsec
W['gait'] = energetic.gait
W['subject'] = energetic.subject

W.to_pickle('./W')