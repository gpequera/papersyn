import pycircstat
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

file = './CoAstats.npy'
coa = np.load(file,encoding='latin1',allow_pickle=True).item()

matching030 = [s for s in list(coa['C1'].columns) if "030" in s]
matching040 = [s for s in list(coa['C1'].columns) if "040" in s]
matching050 = [s for s in list(coa['C1'].columns) if "050" in s]
matching065 = [s for s in list(coa['C1'].columns) if "065" in s]
matching090 = [s for s in list(coa['C1'].columns) if "090" in s]
matching110 = [s for s in list(coa['C1'].columns) if "110" in s]

matching = [matching030,
            matching040 ,
            matching050 ,
            matching065 ,
            matching090 ,
            matching110]

fig, ax = plt.subplots(4,6,figsize=(14, 12),subplot_kw=dict(projection='polar'))
plt.subplots_adjust(wspace=.7)
for ii in range(4):
    for mm,nn in zip(matching,range(len(matching))):
        for match in  mm:
            meanCoA = pycircstat.mean(coa['C'+str(ii+1)][match],axis=0)
            stdCoA = pycircstat.std(coa['C'+str(ii+1)][match],axis=0)
            if 'W' in match:
                colors = "#1B9E77"
                label = match[0]
            elif 'R' in match:
                colors = "#D95F02"
                label = match[0]
            elif 'lead' in match:
                colors = "#7570B3"
                label = match[0]+match[6:-1]
            else:
                colors = '#E7298A'
                label = match[0]+match[6:-1]
            ax[ii,nn].plot([meanCoA,meanCoA],[0,1],label= label,color=colors)
            ax[ii,nn].bar([meanCoA,meanCoA],[0,1], width=stdCoA, bottom=0.0,color = colors,alpha=0.3)
            ax[ii,nn].set_yticks([])
            ax[ii,nn].set_xticks([0,np.pi/2,np.pi,3*(np.pi/2)])
        if ii==0:
            ax[ii,nn].set_title(str(np.round(np.float(match[2:5])/10/3.6,2))+ " m.sec$^{{-1}}$\n\n\n",fontsize=18)
            ax[ii,nn].set_xticklabels([])
            if nn==0:              
                ax[ii,nn].set_xticklabels(['0%','25%','50%','75%'],fontsize=16)
        else:
            ax[ii,nn].set_xticklabels([])
        if nn==0:
            ax[ii,nn].text(np.pi,3,'C'+str(ii+1),fontsize=20)
ax[0,3].legend(bbox_to_anchor=(7, 1),fontsize=20)

ax[0,2].text(np.pi/2,1.2,'*W vs Str\n *Slead vs Str',horizontalalignment='center',fontsize=14)
ax[0,3].text(np.pi/2,1.2,'*W vs Str\n *Slead vs Str\n *R vs Str',horizontalalignment='center',fontsize=14)
ax[0,4].text(np.pi/2,1.2,'*R vs Str\n *Slead vs Str',horizontalalignment='center',fontsize=14)
ax[1,2].text(np.pi/2,1.2,'*W vs Str\n *W vs Slead\n Slead vs Str',horizontalalignment='center',fontsize=14)
ax[1,3].text(np.pi/2,1.2,'*W vs Str\n  *W vs Slead\n *W vs R\n *R vs Str\n *Str vs Slead',horizontalalignment='center',fontsize=14)
ax[1,4].text(np.pi/2,1.2,'*Str vs Slead',horizontalalignment='center',fontsize=14)
ax[2,2].text(np.pi/2,1.2,'*W vs Str\n*W vs Slead',horizontalalignment='center',fontsize=14)
ax[2,3].text(np.pi/2,1.2,'*W vs Str\n*W vs Slead\nR vs Slead\nR vs Str',horizontalalignment='center',fontsize=14)
ax[2,4].text(np.pi/2,1.2,'*R vs Slead\n*R vs Str',horizontalalignment='center',fontsize=14)
ax[3,4].text(np.pi/2,1.2,'*R vs Str',horizontalalignment='center',fontsize=14)

fig.savefig('./fig6RE.pdf',bbox_inches="tight",dpi=300)



